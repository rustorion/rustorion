all: test
test:
	cargo build
	cargo test -- --nocapture
	cd test && make
format:
	cargo fmt
clippy:
	cargo clippy --tests -- -A clippy::significant_drop_in_scrutinee -A clippy::format_push_string -A clippy::get_first

ci: format clippy test
.PHONY: test
