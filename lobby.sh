#!/bin/sh
set -e
cargo build
PLAYER_ID=`cargo run --bin rustorion-cli -- auth ensure default-server`
cd rustorion-gui
cargo run --bin rustorion-gui-client -- create-server -p AI -p $PLAYER_ID create-lobby
