use serde::{Deserialize, Serialize};

#[derive(Eq, PartialEq, Clone, Serialize, Deserialize, Hash, Copy, Debug)]
pub struct Color {
	pub red: u8,
	pub green: u8,
	pub blue: u8,
}

impl Default for Color {
	fn default() -> Self {
		Color { red: 255, green: 255, blue: 255 }
	}
}

impl From<(f64, f64, f64)> for Color {
	fn from(components: (f64, f64, f64)) -> Self {
		assert!(components.0 >= 0.0 && components.0 <= 1.0);
		assert!(components.1 >= 0.0 && components.1 <= 1.0);
		assert!(components.2 >= 0.0 && components.2 <= 1.0);
		Color {
			red: (components.0 * 255.).round() as u8,
			green: (components.1 * 255.).round() as u8,
			blue: (components.2 * 255.).round() as u8,
		}
	}
}

impl From<(u8, u8, u8)> for Color {
	fn from(components: (u8, u8, u8)) -> Self {
		Color {
			red: components.0,
			green: components.1,
			blue: components.2,
		}
	}
}

// For GTK
impl From<Color> for (f64, f64, f64) {
	fn from(color: Color) -> (f64, f64, f64) {
		(color.red as f64 / 255.0, color.green as f64 / 255.0, color.blue as f64 / 255.0)
	}
}

// Also for GTK. I hate GTK.
impl From<Color> for (f32, f32, f32) {
	fn from(color: Color) -> (f32, f32, f32) {
		(color.red as f32 / 255.0, color.green as f32 / 255.0, color.blue as f32 / 255.0)
	}
}

impl Color {
	pub fn to_html(&self) -> String {
		format!("#{:02x}{:02x}{:02x}", self.red, self.green, self.blue)
	}
}

// 16 distinctive colors, 20-90 luminosity
// https://mokole.com/palette.html
pub const DISTINCTIVE_COLORS: [Color; 16] = [
	Color { red: 47, green: 79, blue: 79 },
	Color { red: 107, green: 142, blue: 35 },
	Color { red: 127, green: 0, blue: 0 },
	Color { red: 0, green: 0, blue: 128 },
	Color { red: 255, green: 0, blue: 0 },
	Color { red: 255, green: 165, blue: 0 },
	Color { red: 127, green: 255, blue: 0 },
	Color { red: 0, green: 250, blue: 154 },
	Color { red: 233, green: 150, blue: 122 },
	Color { red: 0, green: 0, blue: 255 },
	Color { red: 255, green: 0, blue: 255 },
	Color { red: 30, green: 144, blue: 255 },
	Color { red: 255, green: 255, blue: 84 },
	Color { red: 221, green: 160, blue: 221 },
	Color { red: 176, green: 224, blue: 230 },
	Color { red: 255, green: 20, blue: 147 },
];
