// frontend for manipulation of UniverseView data
// Intended to be type-safe, assuming you don't mix two different EntityStored objects
// code layout: put view-exclusive functions and types last, put code related to a type together

use crate::action;
use crate::storage::*;
use crate::units::*;
use crate::universeview;
use std::hash::{Hash, Hasher};

pub enum WinState<'a> {
	Winner(Empire<'a>),
	Draw,
	NoWinner,
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct StarSystem<'a> {
	pub data: &'a universeview::StarSystem,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for StarSystem<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.data.name)
	}
}

impl<'a> Hash for StarSystem<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> StarSystem<'a> {
	pub fn can_build_fleet(self, empire: Empire<'a>) -> bool {
		self.empire() == Some(empire)
	}

	/// Is system controlled enough to be captured
	pub fn can_capture(self, empire: Empire<'a>) -> bool {
		let fleets = self.fleets();
		!fleets.is_empty() && fleets.iter().all(|s| s.empire() == empire)
	}

	pub fn is_capital(self) -> bool {
		self.interface.universe.capitals_in_empires.parent(self.data.id).is_some()
	}

	pub fn empire(self) -> Option<Empire<'a>> {
		self.interface.universe.star_systems_in_empires.parent(self.data.id).map(|e_id| self.interface.empire(e_id))
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.interface
			.universe
			.fleets_in_star_systems
			.children(self.data.id)
			.iter()
			.map(|id| self.interface.fleet(*id))
			.collect()
	}

	pub fn fleets_of(self, empire: Empire) -> Vec<Fleet<'a>> {
		self.fleets().into_iter().filter(|fleet| fleet.empire() == empire).collect()
	}

	pub fn starlanes(self) -> Vec<StarSystem<'a>> {
		self.interface.universe.starlanes.links(self.data.id).into_iter().map(|id| self.interface.star_system(id)).collect()
	}

	pub fn starlane_to(self, other: StarSystem) -> bool {
		self.interface.universe.starlanes.linked(self.data.id, other.data.id)
	}

	pub fn population(self) -> Unit<Population> {
		self.planets().iter().map(|p| p.data.population).sum()
	}

	pub fn planets(self) -> Vec<Planet<'a>> {
		self.interface
			.universe
			.planets_in_star_systems
			.children(self.data.id)
			.into_iter()
			.map(|id| self.interface.planet(id))
			.collect()
	}

	pub fn capture_price(self) -> Unit<EnergyCredit> {
		let multiplier = if self.empire().is_some() { 2.0 } else { 1.0 };
		self.population().mul(multiplier).mul(5.0).into()
	}

	pub fn related_migration_events(self) -> Vec<MigrationEvent<'a>> {
		self.interface
			.migration_events()
			.into_iter()
			.filter(|e| e.source_planet().star_system() == self || e.target_planet().star_system() == self)
			.collect()
	}

	/// Is this a system of the current player
	pub fn is_own(self) -> bool {
		self.interface.controlled_empire().is_some() && self.empire() == self.interface.controlled_empire()
	}

	pub fn breach_shipping_cost(self, linked_star_system: Self, empire: Option<Empire>) -> Option<Unit<EnergyCredit>> {
		if self.starlane_to(linked_star_system) {
			let same_empire = linked_star_system.empire() == self.empire();
			let own_empire = linked_star_system.empire() == empire;
			let multiplier: Rational = if same_empire && own_empire {
				(1, 1)
			} else if same_empire {
				(3, 2)
			} else {
				(2, 1)
			}
			.into();
			let cost = self.interface.universe.breach_shipping_cost.mul(multiplier).into();
			Some(cost)
		} else {
			None
		}
	}

	pub fn shipping_path_cost(self, linked_star_system: Self, empire: Option<Empire<'a>>) -> Option<(Vec<StarSystem<'a>>, Unit<EnergyCredit>)> {
		let path_cost = |path: &[ID<universeview::StarSystem>]| {
			let none: Option<ID<universeview::StarSystem>> = None;
			let (_, cost) = path.iter().fold((none, Unit::from(0)), |(prev_star_system_option, sum), next_star_system_id| {
				let next_sum = match prev_star_system_option {
					None => sum,
					Some(prev_star_system_id) => {
						let prev_star_system = self.interface.star_system(prev_star_system_id);
						let next_star_system = self.interface.star_system(*next_star_system_id);
						prev_star_system.breach_shipping_cost(next_star_system, empire).unwrap().add(sum)
					}
				};
				(Some(*next_star_system_id), next_sum)
			});
			cost
		};

		let parents = self.interface.universe.trading_parents_maps.get(&empire.map(|e| e.data.id)).unwrap().get(&self.data.id).unwrap();
		match pathfinding::directed::dijkstra::build_path(&linked_star_system.data.id, parents) {
			path if path.is_empty() => None,
			path => {
				let cost = path_cost(&path);
				Some((path.iter().map(|id| self.interface.star_system(*id)).collect(), cost))
			}
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Empire<'a> {
	pub data: &'a universeview::Empire,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for Empire<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} empire", self.data.name)
	}
}

impl<'a> Hash for Empire<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Empire<'a> {
	pub fn star_systems(self) -> Vec<StarSystem<'a>> {
		self.interface
			.universe
			.star_systems_in_empires
			.children(self.data.id)
			.iter()
			.map(|id| self.interface.star_system(*id))
			.collect()
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.interface
			.universe
			.fleets_in_empires
			.children(self.data.id)
			.into_iter()
			.map(|s_id| self.interface.fleet(s_id))
			.collect()
	}

	pub fn capital(self) -> Option<StarSystem<'a>> {
		let id = self.interface.universe.capitals_in_empires.child(self.data.id);
		id.map(|id| self.interface.star_system(id))
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Fleet<'a> {
	pub data: &'a universeview::Fleet,
	pub interface: universeview::interface::Universe<'a>,
}

impl<'a> std::fmt::Debug for Fleet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Fleet").field("id", &self.data.id).field("name", &self.data.name).finish()
	}
}

impl<'a> std::fmt::Display for Fleet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Fleet {}", self.data.name)
	}
}

impl<'a> std::hash::Hash for Fleet<'a> {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Fleet<'a> {
	pub fn can_build_ships(self) -> bool {
		self.star_system().empire() == Some(self.empire())
	}

	pub fn empire(self) -> Empire<'a> {
		let id = self.interface.universe.fleets_in_empires.parent(self.data.id).expect("Internal inconsistency");
		self.interface.empire(id)
	}

	pub fn star_system(self) -> StarSystem<'a> {
		let id = self.interface.universe.fleets_in_star_systems.parent(self.data.id).expect("Internal inconsistency");
		self.interface.star_system(id)
	}

	pub fn ships(self) -> Vec<Ship<'a>> {
		self.interface.universe.ships_in_fleets.children(self.data.id).iter().map(|id| self.interface.ship(*id)).collect()
	}

	pub fn economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(universeview::EconomyAgent::Fleet(self.data.id))
	}
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Ship<'a> {
	pub data: &'a universeview::Ship,
	pub interface: universeview::interface::Universe<'a>,
}

impl<'a> std::fmt::Debug for Ship<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Ship").field("id", &self.data.id).field("name", &self.data.name).finish()
	}
}

impl<'a> std::hash::Hash for Ship<'a> {
	fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Ship<'a> {
	pub fn fleet(self) -> Fleet<'a> {
		let id = self.interface.universe.ships_in_fleets.parent(self.data.id).expect("Internal inconsistency");
		self.interface.fleet(id)
	}
}

#[derive(Copy, Clone, Debug)]
pub struct Universe<'a> {
	pub universe: &'a universeview::Universe,
}

impl<'a> PartialEq for Universe<'a> {
	fn eq(&self, other: &Self) -> bool {
		self.universe.turn_number == other.universe.turn_number // FIXME: in practice should work...
	}
}

impl<'a> Eq for Universe<'a> {}

impl<'a> Universe<'a> {
	pub fn fleet_cost(self) -> Unit<EnergyCredit> {
		self.universe.ship_cost.mul(2)
	}

	pub fn new(universe: &'a universeview::Universe) -> Self {
		Self { universe }
	}

	pub fn planet(self, id: ID<universeview::Planet>) -> Planet<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Planet { data, interface: self }
	}

	pub fn star_system(self, id: ID<universeview::StarSystem>) -> StarSystem<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		StarSystem { data, interface: self }
	}

	pub fn empire(self, id: ID<universeview::Empire>) -> Empire<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Empire { data, interface: self }
	}

	pub fn ship(self, id: ID<universeview::Ship>) -> Ship<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Ship { data, interface: self }
	}

	pub fn fleet(self, id: ID<universeview::Fleet>) -> Fleet<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		Fleet { data, interface: self }
	}

	pub fn trade_transaction(self, id: ID<universeview::TradeTransaction>) -> TradeTransaction<'a> {
		let data = self.universe.get(id).expect("ID lookup failed");
		TradeTransaction { data, interface: self }
	}

	pub fn fleets(self) -> Vec<Fleet<'a>> {
		self.universe.fleets.iter().map(|x| Fleet { interface: self, data: x.1 }).collect()
	}

	pub fn empires(self) -> Vec<Empire<'a>> {
		self.universe.empires.iter().map(|x| Empire { interface: self, data: x.1 }).collect()
	}

	pub fn star_systems(self) -> Vec<StarSystem<'a>> {
		self.universe.star_systems.iter().map(|x| StarSystem { interface: self, data: x.1 }).collect()
	}

	pub fn planets(self) -> Vec<Planet<'a>> {
		self.universe.planets.iter().map(|x| Planet { interface: self, data: x.1 }).collect()
	}

	pub fn controlled_empire(self) -> Option<Empire<'a>> {
		self.universe.controlled_empire.map(|e_id| self.empire(e_id))
	}

	pub fn win_state(self) -> WinState<'a> {
		match self.universe.win_state {
			crate::universeview::WinState::Winner(empire_id) => WinState::Winner(self.empire(empire_id)),
			crate::universeview::WinState::NoWinner => WinState::NoWinner,
			crate::universeview::WinState::Draw => WinState::Draw,
		}
	}

	pub fn trade_transactions(self) -> Vec<TradeTransaction<'a>> {
		EntityStored::<universeview::TradeTransaction>::storage(self.universe)
			.values()
			.map(|t| TradeTransaction { data: t, interface: self })
			.collect()
	}

	pub fn migration_events(self) -> Vec<MigrationEvent<'a>> {
		self.universe.migration_events.iter().map(|t| MigrationEvent { data: t, interface: self }).collect()
	}

	pub fn committed_resources(self, actions: &[action::Action]) -> CommittedResources<'a> {
		let mut resources = CommittedResources {
			fleets: Default::default(),
			energy_credits: 0.into(),
		};
		for action in actions {
			match action {
				action::Action::BuildShips { ships_number, .. } => {
					resources.energy_credits = resources.energy_credits.add(&self.universe.ship_cost.mul(ships_number));
				}
				action::Action::CaptureStarSystem { star_system_id, .. } => {
					resources.energy_credits = resources.energy_credits.add(&self.star_system(*star_system_id).capture_price());
				}
				action::Action::MoveFleet { fleet_id, .. } => {
					resources.fleets.insert(self.fleet(*fleet_id));
				}
				action::Action::BuildFleets { fleets_number, .. } => {
					resources.energy_credits = resources.energy_credits.add(self.fleet_cost().mul(TryInto::<i64>::try_into(*fleets_number).unwrap()));
				}
			}
		}
		resources
	}

	pub fn economy_agent(self, economy_agent: universeview::EconomyAgent) -> EconomyAgent<'a> {
		match economy_agent {
			universeview::EconomyAgent::Planet(id) => EconomyAgent::Planet(self.planet(id)),
			universeview::EconomyAgent::Fleet(id) => EconomyAgent::Fleet(self.fleet(id)),
			universeview::EconomyAgent::Government(id) => EconomyAgent::Government(self.empire(id)),
			universeview::EconomyAgent::VoidChurchBranch(empire_id, star_system_id) => EconomyAgent::VoidChurchBranch(self.empire(empire_id), self.star_system(star_system_id)),
		}
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Planet<'a> {
	pub data: &'a universeview::Planet,
	pub interface: Universe<'a>,
}

impl<'a> Hash for Planet<'a> {
	fn hash<H: Hasher>(&self, state: &mut H) {
		self.data.id.hash(state);
	}
}

impl<'a> Planet<'a> {
	pub fn star_system(self) -> StarSystem<'a> {
		let id = self.interface.universe.planets_in_star_systems.parent(self.data.id).expect("Internal inconsistency");
		self.interface.star_system(id)
	}

	/// Energy cost to produce 1 food ration without additional infrastructure
	pub fn agriculture_cost(self) -> Rational {
		Rational::from((1, 10))
			+ Rational::from((125, 1000)) * (Rational::from(1).sub(Rational::from(self.data.atmosphere_bio_quality)))
			+ Rational::from(2) * (Rational::from(1).sub(Rational::from(self.data.surface_bio_quality)))
	}

	// Cost of insurance for 1 human living on the planet
	// Energy is considered to be spent locally to cover living costs
	pub fn insurance_cost(self) -> Rational {
		let base_cost = Rational::from((1, 100));

		let local_cosmic_ray_intensity: Rational = self.star_system().data.star_cosmic_ray_intensity.into();
		let core_spin_compensation: Rational = self.data.core_spin_speed.into();
		let effective_cosmic_ray_intensity: Rational = local_cosmic_ray_intensity.sub(core_spin_compensation).max(0.into());
		let cosmic_ray_insurance = effective_cosmic_ray_intensity.div(1000);

		let surface_bio_quality_offset: Rational = Rational::from(1).sub(Rational::from(self.data.surface_bio_quality));
		let surface_bio_quality_insurance = surface_bio_quality_offset.div(10);

		let atmosphere_bio_quality_offset: Rational = Rational::from(1).sub(Rational::from(self.data.atmosphere_bio_quality));
		let atmosphere_bio_quality_insurance = Rational::from(atmosphere_bio_quality_offset).div(10);

		// tracing::warn!("{}", base_cost + cosmic_ray_insurance + surface_bio_quality_insurance + atmosphere_bio_quality_insurance);
		base_cost + cosmic_ray_insurance + surface_bio_quality_insurance + atmosphere_bio_quality_insurance
	}

	pub fn income_tax(self) -> Option<(Rational, Empire<'a>)> {
		self.star_system().empire().map(|e| (Rational::from((e.data.tax_rate as i128, 100)), e))
	}

	// Effective cost to produce 1 Fuel
	pub fn fuel_cost_price(self) -> Rational {
		Rational::from(1).div(self.data.fuel_availability).mul(self.insurance_cost())
	}

	/// food consumed by 1 population
	pub fn food_consumption(self) -> Rational {
		1.into()
	}

	pub fn related_migration_events(self) -> Vec<MigrationEvent<'a>> {
		self.interface
			.migration_events()
			.into_iter()
			.filter(|e| e.source_planet() == self || e.target_planet() == self)
			.collect()
	}

	pub fn economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(universeview::EconomyAgent::Planet(self.data.id))
	}
}

impl<'a> std::fmt::Display for Planet<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let star_system_name = &self.star_system().data.name;
		write!(f, "{star_system_name} {}", self.data.number)
	}
}
#[derive(PartialEq, Eq, Clone, Copy)]
pub struct TradeTransaction<'a> {
	pub data: &'a universeview::TradeTransaction,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for TradeTransaction<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let (price, fee_price) = match self.data.cargo {
			universeview::Cargo::Food(food) => (self.data.income.div(f64::from(food)), self.data.shipping_fee.div(f64::from(food))),
			universeview::Cargo::Fuel(fuel) => (self.data.income.div(f64::from(fuel)), self.data.shipping_fee.div(f64::from(fuel))),
		};
		write!(
			f,
			"{} sold {} to {} for {} (price: {:.3}) (shipping: {}, price: {:.3})",
			self.source_economy_agent(),
			self.data.cargo,
			self.target_economy_agent(),
			self.data.income,
			price,
			self.data.shipping_fee,
			fee_price,
		)
	}
}

impl<'a> TradeTransaction<'a> {
	fn source_economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(self.data.source_economy_agent)
	}

	fn target_economy_agent(self) -> EconomyAgent<'a> {
		self.interface.economy_agent(self.data.target_economy_agent)
	}
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub struct MigrationEvent<'a> {
	pub data: &'a universeview::MigrationEvent,
	pub interface: Universe<'a>,
}

impl<'a> std::fmt::Display for MigrationEvent<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{} migrated from {} to {} for {}", self.data.population, self.source_planet(), self.target_planet(), self.data.fee,)
	}
}

impl<'a> MigrationEvent<'a> {
	fn source_planet(self) -> Planet<'a> {
		self.interface.planet(self.data.source_planet_id)
	}

	fn target_planet(self) -> Planet<'a> {
		self.interface.planet(self.data.target_planet_id)
	}
}

#[derive(Clone, Hash, PartialEq, Eq, Copy)]
pub enum EconomyAgent<'a> {
	Planet(Planet<'a>),
	Fleet(Fleet<'a>),
	Government(Empire<'a>),
	VoidChurchBranch(Empire<'a>, StarSystem<'a>),
}

impl<'a> From<EconomyAgent<'a>> for universeview::EconomyAgent {
	fn from(economy_agent: EconomyAgent<'a>) -> Self {
		match economy_agent {
			EconomyAgent::Planet(planet) => universeview::EconomyAgent::Planet(planet.data.id()),
			EconomyAgent::Fleet(fleet) => universeview::EconomyAgent::Fleet(fleet.data.id()),
			EconomyAgent::Government(empire) => universeview::EconomyAgent::Government(empire.data.id()),
			EconomyAgent::VoidChurchBranch(empire, star_system) => universeview::EconomyAgent::VoidChurchBranch(empire.data.id(), star_system.data.id()),
		}
	}
}

impl<'a> EconomyAgent<'a> {
	pub fn star_system(self) -> StarSystem<'a> {
		match self {
			EconomyAgent::Planet(planet) => planet.star_system(),
			EconomyAgent::Fleet(fleet) => fleet.star_system(),
			EconomyAgent::Government(empire) => empire.capital().unwrap(),
			EconomyAgent::VoidChurchBranch(_, star_system) => star_system,
		}
	}

	pub fn empire(self) -> Option<Empire<'a>> {
		match self {
			EconomyAgent::Planet(planet) => planet.star_system().empire(),
			EconomyAgent::Fleet(fleet) => Some(fleet.empire()),
			EconomyAgent::Government(empire) => Some(empire),
			EconomyAgent::VoidChurchBranch(empire, _) => Some(empire),
		}
	}

	pub fn related_trade_transactions(self) -> Vec<TradeTransaction<'a>> {
		self.star_system()
			.interface
			.universe
			.trade_transaction_data
			.index_economy_agent
			.map
			.get(&self.into())
			.map(|hs| hs.iter().map(|id| self.star_system().interface.trade_transaction(*id)).collect())
			.unwrap_or_default()
	}
}

impl<'a> std::fmt::Display for EconomyAgent<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			EconomyAgent::Planet(planet) => write!(f, "planet {}", planet),
			EconomyAgent::Fleet(fleet) => write!(f, "fleet {}", fleet),
			EconomyAgent::Government(empire) => write!(f, "{} government", empire),
			EconomyAgent::VoidChurchBranch(empire, _) => write!(f, "{} void church branch", empire),
		}
	}
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CommittedResources<'a> {
	pub fleets: std::collections::HashSet<Fleet<'a>>,
	pub energy_credits: Unit<EnergyCredit>,
}
