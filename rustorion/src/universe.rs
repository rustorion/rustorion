pub mod interface;

// Low-level universe data storage
// Use-cases: modification of universe data (higher interfaces are read-only)
//            building high-level interfaces

use crate::action;
use crate::storage::links;
use crate::storage::{EntityStored, IDentified, IssuesIDs, StorageID, ID};
use crate::units::*;
use anyhow::Result;
use blowfish::Blowfish;
use byteorder::LittleEndian;
use cipher::BlockEncrypt;
use cipher::KeyInit;
use num_traits::*;
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(PartialEq, Eq, Clone, Copy, Serialize, Deserialize, Hash, Debug, Default)]
pub enum WinState {
	Winner(ID<Empire>),
	Draw,
	#[default]
	NoWinner,
}

#[derive(PartialEq, Eq, PartialOrd, Clone, Copy, Serialize, Deserialize, Hash, Debug)]
pub struct UniverseLocation {
	pub x: i64,
	pub y: i64,
}

impl From<(i64, i64)> for UniverseLocation {
	fn from(a: (i64, i64)) -> Self {
		UniverseLocation { x: a.0, y: a.1 }
	}
}

impl From<UniverseLocation> for (i64, i64) {
	fn from(universe_location: UniverseLocation) -> (i64, i64) {
		(universe_location.x, universe_location.y)
	}
}

impl core::ops::Add for UniverseLocation {
	type Output = Self;

	fn add(self, other: Self) -> Self::Output {
		UniverseLocation {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}

#[derive(PartialEq, Clone, Eq, Serialize, Deserialize, Debug, Default)]
pub struct Planet {
	pub id: ID<Planet>,
	pub core_spin_speed: Unit<CoreSpinSpeed>,
	pub surface_bio_quality: Unit<SurfaceBioQuality>,
	pub atmosphere_bio_quality: Unit<AtmosphereBioQuality>,
	pub fuel_availability: Rational,
	pub population: Unit<Population>,
	pub number: i64,
	pub goods_storage: GoodsStorage,
	pub planetary_economics: PlanetaryEconomics,
	pub economic_asset_list: EconomicAssetList,
}

impl Planet {
	pub fn set_natural_fuel_availability(&mut self) {
		self.fuel_availability = Rational::from(1).sub(Rational::from(self.core_spin_speed)).max((1, 1_000_000).into());
	}
}

impl IDentified for Planet {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Planet> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Planet> {
		&self.planets
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Planet> {
		&mut self.planets
	}
}

#[derive(PartialEq, Eq, Clone, Serialize, Copy, Debug, Deserialize)]
pub enum StarType {
	/// White Dwarf -- "ocean", logistics friendly
	White,
	/// G-Type/yellow dwarf -- agricultural/habitation
	Yellow,
	/// Red Giant -- industry
	Red,
	/// Neutron stars -- exotic conditions, science
	Blue,
}

impl std::fmt::Display for StarType {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
		let s = match self {
			StarType::White => "White Dwarf",
			StarType::Yellow => "G-Type",
			StarType::Red => "Red Giant",
			StarType::Blue => "Neutron",
		};
		write!(fmt, "{}", s)
	}
}

#[derive(PartialEq, Eq, Clone, Serialize, Debug, Deserialize)]
pub struct StarSystem {
	pub id: ID<StarSystem>,
	pub star_type: StarType,
	pub name: String,
	pub location: UniverseLocation,
	pub star_cosmic_ray_intensity: Unit<CosmicRayIntensity>,
}

impl IDentified for StarSystem {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<StarSystem> for Universe {
	fn storage(&self) -> &HashMap<StorageID, StarSystem> {
		&self.star_systems
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, StarSystem> {
		&mut self.star_systems
	}
}

#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize, Default)]
pub struct Empire {
	pub id: ID<Empire>,
	pub name: String,
	pub color: crate::color::Color,
	pub submitted_actions: Option<Vec<action::Action>>,
	// percentage, to avoid precision issues
	pub tax_rate: u64,
	pub empire_economics: EmpireEconomics,
	pub goods_storage: GoodsStorage,
}

impl IDentified for Empire {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Empire> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Empire> {
		&self.empires
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Empire> {
		&mut self.empires
	}
}

#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize, Default)]
pub struct EmpireEconomics {
	pub tax_income: Unit<EnergyCredit>,
}

#[derive(PartialEq, Eq, Debug, Serialize, Clone, Deserialize)]
pub struct Fleet {
	pub id: ID<Fleet>,
	pub name: String,
	pub goods_storage: GoodsStorage,
}

impl IDentified for Fleet {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Fleet> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Fleet> {
		&self.fleets
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Fleet> {
		&mut self.fleets
	}
	fn unindex(&mut self, fleet_id: ID<Fleet>) -> Result<()> {
		self.fleets_in_star_systems.purge_child(fleet_id);
		self.fleets_in_empires.purge_child(fleet_id);
		self.ships_in_fleets.purge_parent(fleet_id);
		Ok(())
	}
}

#[derive(PartialEq, Eq, Debug, Serialize, Clone, Copy, Deserialize)]
pub enum ShipModel {
	Box,
}

impl ShipModel {
	fn crew_capacity(self) -> Unit<Population> {
		match self {
			ShipModel::Box => Unit::from_microunits(10),
		}
	}

	/// Price of ship's production, relation to the Universe::ship_cost()
	fn price(self) -> Rational {
		match self {
			ShipModel::Box => 1.into(),
		}
	}

	fn maintenance_cost(self) -> GoodsStorage {
		GoodsStorage {
			food: self.crew_capacity().microunits.into(),
			fuel: 100.into(),
			..Default::default()
		}
	}
}

#[derive(PartialEq, Eq, Debug, Serialize, Clone, Deserialize)]
pub struct Ship {
	pub id: ID<Ship>,
	pub name: String,
	pub model: ShipModel,
}

impl IDentified for Ship {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Ship> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Ship> {
		&self.ships
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Ship> {
		&mut self.ships
	}
	fn unindex(&mut self, ship_id: ID<Ship>) -> Result<()> {
		self.ships_in_fleets.purge_child(ship_id);
		Ok(())
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct Universe {
	pub planets: HashMap<StorageID, Planet>,
	pub star_systems: HashMap<StorageID, StarSystem>,

	current_id: StorageID,
	pub empires: HashMap<StorageID, Empire>,
	pub planets_in_star_systems: links::HasMany<StarSystem, Planet>,
	pub starlanes: links::Interlink<StarSystem>,
	pub star_systems_in_empires: links::HasMany<Empire, StarSystem>,
	pub ships: HashMap<StorageID, Ship>,
	pub fleets_in_star_systems: links::HasMany<StarSystem, Fleet>,
	pub fleets_in_empires: links::HasMany<Empire, Fleet>,
	pub id_storage_key: [u8; 8],
	pub turn_number: u64,
	pub capitals_in_empires: links::HasOne<Empire, StarSystem>,
	pub win_state: WinState,
	pub width: u64,
	pub height: u64,
	/// Energy cost to produce one Box with basic equipment and enough manpower.
	pub ship_cost: Unit<EnergyCredit>,
	/// Pathfinding information for trading
	pub trading_parents_maps: TradingParentsMaps,
	pub migration_events: Vec<MigrationEvent>,
	pub ships_in_fleets: links::HasMany<Fleet, Ship>,
	pub fleets: HashMap<StorageID, Fleet>,
	/// Basic cost to transfer 10 cm^3 through a breach between two systems using commercial bulk transport, EC/cm3
	pub breach_shipping_cost: Rational,
	pub void_church: VoidChurch,
	pub trade_transaction_data: TradeTransactionData,
}

impl std::fmt::Debug for Universe {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Universe").finish()
	}
}

impl Default for Universe {
	fn default() -> Self {
		let mut id_storage_key: [u8; 8] = <[u8; 8]>::default();
		rand::thread_rng().fill(&mut id_storage_key);
		Universe {
			planets: Default::default(),
			star_systems: Default::default(),
			current_id: 0,
			empires: Default::default(),
			ships: Default::default(),
			planets_in_star_systems: Default::default(),
			fleets_in_star_systems: Default::default(),
			fleets_in_empires: Default::default(),
			starlanes: Default::default(),
			star_systems_in_empires: Default::default(),
			capitals_in_empires: Default::default(),
			id_storage_key,
			turn_number: 0,
			win_state: WinState::NoWinner,
			width: 1000,
			height: 1000,
			ship_cost: 1_000_000.into(),
			breach_shipping_cost: 1.into(),
			trading_parents_maps: Default::default(),
			migration_events: Default::default(),
			ships_in_fleets: Default::default(),
			fleets: Default::default(),
			void_church: Default::default(),
			trade_transaction_data: Default::default(),
		}
	}
}

impl IssuesIDs for Universe {
	fn next_id(&mut self) -> StorageID {
		self.current_id += 1;
		let mut bytes = self.current_id.to_le_bytes().into();
		let blowfish = Blowfish::<LittleEndian>::new_from_slice(&self.id_storage_key).unwrap();
		blowfish.encrypt_block(&mut bytes);
		u64::from_le_bytes(bytes.into())
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Default)]
pub struct TradeTransactionData {
	pub storage: HashMap<StorageID, TradeTransaction>,
	pub index_turn: crate::storage::index::UnorderedUniqueIndex<u64, TradeTransaction>,
	pub index_economy_agent: crate::storage::index::UnorderedUniqueIndex<EconomyAgent, TradeTransaction>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Default)]
/// currently an unique entity working as a bank
pub struct VoidChurch {
	pub goods_storage: GoodsStorage,
}

impl VoidChurch {
	fn trading_data(&self) -> TradingData {
		TradingData {
			food: TradedGoodData { ..Default::default() },
			// As the government issues EC, it will buy as much as possible for 1.0 and sell as much as possible (for now) for 1.0, not including shipping. This assumes no empires issue unbacked EC.
			fuel: TradedGoodData {
				demands: vec![Demand {
					budget: None,
					quantity: None,
					max_price: 1.into(),
					fixed_price: true,
					issues_energy_credits: true,
					will_not_pay_shipping: true,
				}],
				supplies: vec![Supply {
					quantity: self.goods_storage.fuel,
					min_price: 1.into(),
					fixed_price: true,
					issues_energy_credits: true,
					will_not_pay_shipping: true,
					..Default::default()
				}],
			},
		}
	}
}

type TradingParentsMap = HashMap<ID<StarSystem>, HashMap<ID<StarSystem>, (ID<StarSystem>, Rational)>>;
type TradingParentsMaps = HashMap<Option<ID<Empire>>, TradingParentsMap>;

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub struct TradeTransaction {
	pub source_economy_agent: EconomyAgent,
	pub target_economy_agent: EconomyAgent,
	pub cargo: Cargo,
	pub shipping_fee: Unit<EnergyCredit>,
	pub income: Unit<EnergyCredit>,
	pub turn_number: u64,
	pub id: ID<TradeTransaction>,
}

impl IDentified for TradeTransaction {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<TradeTransaction> for Universe {
	fn storage(&self) -> &HashMap<StorageID, TradeTransaction> {
		&self.trade_transaction_data.storage
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, TradeTransaction> {
		&mut self.trade_transaction_data.storage
	}
	fn index(&mut self, id: ID<TradeTransaction>) -> Result<()> {
		let trade_transaction = *self.getf(id)?;
		self.trade_transaction_data.index_turn.add_index(trade_transaction.turn_number, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.add_index(trade_transaction.source_economy_agent, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.add_index(trade_transaction.target_economy_agent, trade_transaction.id())
	}
	fn unindex(&mut self, id: ID<TradeTransaction>) -> Result<()> {
		let trade_transaction = *self.getf(id)?;
		self.trade_transaction_data.index_turn.remove_index(trade_transaction.turn_number, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.remove_index(trade_transaction.source_economy_agent, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.remove_index(trade_transaction.target_economy_agent, trade_transaction.id())
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub struct MigrationEvent {
	pub source_planet_id: ID<Planet>,
	pub target_planet_id: ID<Planet>,
	pub fee: Unit<EnergyCredit>,
	pub population: Unit<Population>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub enum Cargo {
	Food(Unit<Food>),
	Fuel(Unit<Fuel>),
}

impl std::fmt::Display for Cargo {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Cargo::Food(units) => write!(f, "a shipment of {}", units),
			Cargo::Fuel(units) => write!(f, "a shipment of {}", units),
		}
	}
}

impl Universe {
	pub fn resize_to_bounding_box(&mut self, rate: f64) {
		let mut max: i64 = 0;
		for star_system in self.star_systems.values() {
			max = star_system.location.x.abs().max(max);
			max = star_system.location.y.abs().max(max);
		}
		self.width = (max as f64).mul(rate) as u64;
		self.height = (max as f64).mul(rate) as u64;
	}

	pub fn interface(&self) -> interface::Universe {
		interface::Universe::new(self)
	}

	pub fn set_actions(&mut self, actions: Option<Vec<action::Action>>, actor: &action::Actor) -> Result<(), action::ActionsFailed> {
		let empire_id = actor.empire_id.cast();
		if let Some(actions) = actions.clone() {
			let u_v = self.empire_view(empire_id);
			action::ActionCache::new_batch(actions, &u_v, actor)?;
		};
		let empire = self.getf_mut(empire_id).unwrap();
		empire.submitted_actions = actions;
		Ok(())
	}

	pub fn add_empire(&mut self, name: &str, color: crate::color::Color) -> Result<ID<Empire>> {
		self.insert(Empire {
			color,
			name: String::from(name),
			tax_rate: 30,
			..Default::default()
		})
	}

	pub fn add_planet(&mut self, star_system_id: ID<StarSystem>, planet: Planet) -> Result<ID<Planet>> {
		let planet_id = self.insert(planet)?;
		self.planets_in_star_systems.add_link(star_system_id, planet_id);
		Ok(planet_id)
	}

	pub fn set_star_system_owner(&mut self, star_system_id: ID<StarSystem>, owner_id: ID<Empire>) {
		if let Some(parent) = self.star_systems_in_empires.parent(star_system_id) {
			self.star_systems_in_empires.del_link(parent, star_system_id);
		}
		assert!(self.star_systems_in_empires.add_link(owner_id, star_system_id));
	}

	pub fn add_ship(&mut self, name: &str, fleet_id: ID<Fleet>) -> Result<ID<Ship>> {
		let ship_id = self.insert(Ship {
			id: ID::invalid(),
			name: String::from(name),
			model: ShipModel::Box,
		})?;
		self.ships_in_fleets.add_link(fleet_id, ship_id);
		Ok(ship_id)
	}

	pub fn add_fleet(&mut self, name: &str, star_system_id: ID<StarSystem>, empire_id: ID<Empire>) -> Result<ID<Fleet>> {
		let fleet_id = self.insert(Fleet {
			id: ID::invalid(),
			name: String::from(name),
			goods_storage: Default::default(),
		})?;
		self.fleets_in_empires.add_link(empire_id, fleet_id);
		self.fleets_in_star_systems.add_link(star_system_id, fleet_id);
		Ok(fleet_id)
	}

	pub fn apply_actions(&mut self) {
		let empire_ids: Vec<_> = self.empires.values().map(|e| e.id).collect();
		for empire_id in empire_ids {
			let a = {
				let empire = self.getf_mut(empire_id).unwrap();
				let a = empire.submitted_actions.clone().unwrap_or_default();
				empire.submitted_actions = None;
				a
			};
			let actor = action::Actor { empire_id: empire_id.cast() };
			for action in a {
				action.apply(self, &actor);
			}
		}
	}

	pub fn make_turn(&mut self) -> Result<()> {
		let mut b = crate::util::benchmark::Benchmark::default();
		self.clear_events();
		self.apply_actions();
		b.milestone("actions");
		self.resolve_battles();
		b.milestone("battles");
		self.trade()?;
		b.milestone("trade");
		self.produce();
		b.milestone("production");
		// self.migrate();
		// b.milestone("migration");
		self.check_winner();
		b.milestone("winner");
		self.calculate_trading_parents();
		b.milestone("trading parents");
		self.turn_number += 1;

		let span = tracing::debug_span!("trade");
		let enter = span.enter();
		tracing::debug!("Trade events:");
		for trade_event in &self.interface().trade_transactions() {
			tracing::debug!("{}", trade_event);
		}
		drop(enter);

		let span = tracing::debug_span!("migration");
		let enter = span.enter();
		tracing::debug!("Migration events:");
		for migration_event in &self.interface().migration_events() {
			tracing::debug!("{}", migration_event);
		}
		drop(enter);

		Ok(())
	}

	pub fn clear_events(&mut self) {
		self.migration_events.clear();
	}

	fn perform_trade<TradeableUnit: Tradeable>(&mut self, traded_good_datas: Vec<(EconomyAgent, TradedGoodData<TradeableUnit>)>, mailboxes: &mut HashMap<EconomyAgent, GoodsStorage>) -> Result<()> {
		// we're modifying in the middle here, so trading should not invalidate IDs

		let span = tracing::trace_span!("trade", good = TradeableUnit::UNIT_NAME);
		let enter = span.enter();
		let mut supply_infos: Vec<_> = Default::default();
		let mut demand_infos: Vec<_> = Default::default();
		// goods that are to be added to trading partners's storage are stored here instead and added later in trade(), to avoid inconsistency

		// create all the demand/supply objects
		for (economy_agent, traded_good_data) in traded_good_datas {
			for demand in traded_good_data.demands {
				if (demand.quantity.is_none() || demand.quantity.is_some_and(|q| q > Zero::zero())) && (demand.budget.is_none() || demand.budget.is_some_and(|q| q > Zero::zero())) {
					demand_infos.push(RefCell::new(DemandInfo {
						demand,
						economy_agent,
						supplies: Default::default(),
					}));
				}
			}
			for supply in traded_good_data.supplies {
				if supply.quantity > Zero::zero() {
					supply_infos.push(RefCell::new(SupplyInfo {
						supply,
						economy_agent,
						demands: Default::default(),
					}));
				}
			}
		}

		// fill tables by effective price

		let mut bankrupts: HashSet<EconomyAgent> = Default::default();

		// minimum prices for which there is infinite demand. There is no reason to sell lower than these, because competition cannot be denied.
		let mut supplier_sink_prices: HashMap<EconomyAgent, Rational> = Default::default();

		fn check_for_deal<TradeableUnit: Tradeable>(universe: &Universe, supply_info: &SupplyInfo<TradeableUnit>, demand_info: &DemandInfo<TradeableUnit>) -> Option<(Rational, Rational, Rational)> {
			if supply_info.economy_agent == demand_info.economy_agent {
				return None;
			};
			let supply_partner_interface = universe.interface().economy_agent(supply_info.economy_agent);
			let demand_partner_interface = universe.interface().economy_agent(demand_info.economy_agent);
			let span = tracing::trace_span!(
				"trade",
				partner = supply_partner_interface.to_string(),
				partner = demand_partner_interface.to_string(),
				good = TradeableUnit::UNIT_NAME
			);
			let enter = span.enter();
			let shipping_cost = demand_partner_interface.shipping_cost(supply_partner_interface)?;

			let shipping_price: Rational = shipping_cost.mul(TradeableUnit::cost_per_unit()); // TODO: make an interface method

			tracing::trace!("{supply_partner_interface} -> {demand_partner_interface}");

			// check if these do have any potential at all

			if demand_info.demand.quantity.is_some_and(|v| v.microunits < 100) || supply_info.supply.quantity.microunits < 100 {
				tracing::trace!("Too small: s:d {:?} {}", demand_info.demand.quantity, supply_info.supply.quantity);
				return None;
			};

			// functionally the same as if supplier paid for shipping
			tracing::trace!("{} d:s {} shipping {shipping_price}", demand_info.demand.max_price, supply_info.supply.min_price);
			if demand_info.demand.max_price < supply_info.supply.min_price {
				tracing::trace!("Clearly non-profitable");
				return None;
			};

			let adjusted_supply_min_price = if supply_info.supply.shipping_included {
				supply_info.supply.min_price.sub(shipping_price)
			} else {
				supply_info.supply.min_price
			};

			let (effective_min_price_for_demander, effective_max_price_for_supplier) = match (supply_info.supply.fixed_price, demand_info.demand.fixed_price) {
				(false, false) => (adjusted_supply_min_price.add(shipping_price), demand_info.demand.max_price.sub(shipping_price)),
				(true, false) => (adjusted_supply_min_price.add(shipping_price), adjusted_supply_min_price),
				(false, true) => (demand_info.demand.max_price, demand_info.demand.max_price.sub(shipping_price)),
				// this will perhaps work if the shipping price is zero for some reason
				(true, true) => (demand_info.demand.max_price.add(shipping_price), demand_info.demand.max_price),
			};

			tracing::trace!(
				"demander: {}, supplier: {}",
				effective_min_price_for_demander.floatprint(),
				effective_max_price_for_supplier.floatprint()
			);

			if effective_min_price_for_demander <= demand_info.demand.max_price && effective_max_price_for_supplier >= adjusted_supply_min_price {
				tracing::trace!("feasible.");
				drop(enter);
				Some((effective_max_price_for_supplier, effective_min_price_for_demander, shipping_price))
			} else {
				None
			}
		}

		let sinks_span = tracing::trace_span!("trade_sinks", good = TradeableUnit::UNIT_NAME);
		let sinks_enter = span.enter();

		tracing::trace!("Determining sink prices");

		for supply_info_ref in &supply_infos {
			for demand_info_ref in &demand_infos {
				let supply_info = supply_info_ref.borrow();
				let demand_info = demand_info_ref.borrow();

				if let Some((effective_max_price_for_supplier, _, _)) = check_for_deal(self, supply_info.deref(), demand_info.deref()) {
					if demand_info.demand.issues_energy_credits && demand_info.demand.budget.is_none() && demand_info.demand.fixed_price {
						// it's a sink, record the price (even if it might be the same...)
						supplier_sink_prices
							.entry(supply_info.economy_agent)
							.and_modify(|price| *price = (*price).max(effective_max_price_for_supplier))
							.or_insert(effective_max_price_for_supplier);
					};
				}
			}
		}

		tracing::trace!("Adjusting supply minimum prices considering sinks:");
		for supply_info_ref in &supply_infos {
			let mut supply_info = supply_info_ref.borrow_mut();
			if let Some(sink_price) = supplier_sink_prices.get(&supply_info.economy_agent) {
				supply_info.supply.min_price = supply_info.supply.min_price.max(*sink_price);
				if !sinks_span.is_disabled() {
					tracing::trace!("{}", supply_info.to_string(self));
				}
			}
		}
		drop(sinks_enter);

		// assemble tables of available deals
		for supply_info_ref in &supply_infos {
			for demand_info_ref in &demand_infos {
				let mut supply_info = supply_info_ref.borrow_mut();
				let mut demand_info = demand_info_ref.borrow_mut();

				if let Some((effective_max_price_for_supplier, effective_min_price_for_demander, shipping_price)) = check_for_deal(self, supply_info.deref(), demand_info.deref()) {
					let partner_supply_infos = demand_info.supplies.entry(effective_min_price_for_demander).or_default();
					partner_supply_infos.push((supply_info_ref, shipping_price));

					supply_info.demands.entry(effective_max_price_for_supplier).or_default().push(demand_info_ref);
				}
			}
		}

		let mut loop_number = 0;

		loop {
			let mut deals = 0;
			let mut deals_modified = false;

			let mut dot_file_option = None;
			let turn_number = self.turn_number;

			let trading_tables_enabled = tracing::enabled!(target: "trading_tables", tracing::Level::TRACE);
			let mut dot_log = |string: &str| {
				assert!(trading_tables_enabled);
				let dot_file = dot_file_option
					.get_or_insert_with(|| std::io::BufWriter::new(std::fs::File::create(format!("trading_tables_{}_{}_{}.dot", TradeableUnit::UNIT_NAME, turn_number, loop_number)).unwrap()));

				std::io::Write::write(dot_file, string.as_bytes()).unwrap();
			};

			if trading_tables_enabled {
				// dump the trading tables to a file
				dot_log("digraph { layout=fdp; splines=compound;\n");
				for demand_info_ref in &demand_infos {
					let demand_info = demand_info_ref.borrow();
					let demand_info_str = demand_info.to_string(self);
					dot_log(&format!("subgraph \"cluster {demand_info_str}\" {{ label=\"{demand_info_str}\"; shape=tab; \n"));
					let mut count = 0;
					for (effective_supply_price, supply_infos) in demand_info.supplies.iter() {
						let effective_supply_price_f = effective_supply_price.floatprint();
						dot_log(&format!(
							"subgraph \"cluster {demand_info_str} {effective_supply_price_f:.3}\" {{ label=\"{effective_supply_price_f:.3}\"; shape=tab; \n"
						));
						for (supply_ref, _) in supply_infos.iter() {
							let supply_info = supply_ref.borrow();
							let supply_info_str = supply_info.to_string(self);
							let sink_string = supplier_sink_prices
								.get(&supply_info.economy_agent)
								.map(|price| format!("(sink {})", price.floatprint()))
								.unwrap_or("".into());

							dot_log(&format!(
								"\"{supply_info_str} {demand_info_str}\" [sortv={count}; label=\"{supply_info_str}{sink_string}\"; shape=box ];\n",
							));

							dot_log(&format!("\"{supply_info_str} {demand_info_str}\" -> \"cluster {supply_info_str}\"  [constraint=false;];\n"));
							count.add_assign(1);
						}
						dot_log("}");
					}
					dot_log("}");
				}

				for supply_info_ref in &supply_infos {
					let supply_info = supply_info_ref.borrow();
					let supply_info_str = supply_info.to_string(self);
					dot_log(&format!("\tsubgraph \"cluster {supply_info_str}\" {{ label=\"{supply_info_str}\"; shape=tab;\n"));
					let mut count = 0;
					for (effective_demand_price, demand_infos) in supply_info.demands.iter() {
						let effective_demand_price_f = effective_demand_price.floatprint();
						dot_log(&format!(
							"\t\tsubgraph \"cluster {supply_info_str} {effective_demand_price_f}\" {{ label=\"{effective_demand_price_f}\"; shape=tab; \n"
						));
						for demand_ref in demand_infos.iter() {
							let demand_info = demand_ref.borrow();
							let demand_info_str = demand_info.to_string(self);

							dot_log(&format!("\t\t\t\"{demand_info_str} {supply_info_str}\" [sortv={count}; label=\"{demand_info_str}\"; shape=box ];\n",));
							dot_log(&format!("\"{demand_info_str} {supply_info_str}\" -> \"cluster {demand_info_str}\" [constraint=false];\n"));
							count.add_assign(1);
						}
						dot_log("\t\t}\n");
					}
					dot_log("\t}\n");
				}

				dot_log("}")
			};

			for demand_info_ref in &demand_infos {
				let mut demand_info = demand_info_ref.borrow_mut();

				if bankrupts.contains(&demand_info.economy_agent) {
					continue;
				};

				if demand_info.demand.quantity == Some(Zero::zero())
					|| demand_info.demand.budget == Some(Zero::zero())
					|| (!demand_info.demand.issues_energy_credits && demand_info.economy_agent.goods_storage(self).energy_credits.is_zero())
				{
					bankrupts.insert(demand_info.economy_agent);
					continue;
				};

				let demand_partner_interface = self.interface().economy_agent(demand_info.economy_agent);
				let demand_span = tracing::trace_span!("trade", partner = demand_partner_interface.to_string(), good = TradeableUnit::UNIT_NAME);
				let demand_info_str = demand_info.to_string(self);
				let demand_enter = demand_span.enter();

				tracing::trace!("-- demand: {}", demand_info_str);

				let mut empty_cans = vec![];

				let mut best_supplies = || {
					let mut good_ones = None;
					for (effective_supply_price, supply_infos) in demand_info.supplies.iter_mut() {
						supply_infos.retain_mut(|(supply_ref, _)| {
							let supply_info = supply_ref.borrow();
							// check if the candidate still can deal
							!supply_info.supply.quantity.microunits.is_zero() && !TradeableUnit::unit_storage(supply_info.economy_agent.goods_storage(self)).is_zero()
						});

						if !supply_infos.is_empty() {
							if let Some(best_supplies) = good_ones {
								// we already found good ones, that's second best for price negotiation
								return Some((best_supplies, Some(*effective_supply_price)));
							} else {
								good_ones = Some((*effective_supply_price, supply_infos.clone()));
							}
						} else {
							empty_cans.push(*effective_supply_price);
						}
					}
					good_ones.map(|g_o| (g_o, None))
				};

				let candidates_option = best_supplies();

				for empty_can in &empty_cans {
					demand_info.supplies.remove(empty_can);
				}
				drop(demand_info);
				let ((effective_min_price_for_demander, best_candidates), next_effective_min_price_for_demander_option) = if let Some(candidates) = candidates_option {
					candidates
				} else {
					continue;
				};

				tracing::trace!(
					"minimum price: {effective_min_price_for_demander}, next: {}",
					next_effective_min_price_for_demander_option.map(|price| price.to_string()).unwrap_or_else(|| "no competition".into())
				);

				tracing::trace!("looking for best suppliers among {}", best_candidates.len());

				let mut supply_deals: BTreeMap<Rational, Vec<_>> = Default::default();

				// find the best demand for each of best partners found
				for (best_supply_info_ref, shipping_price) in best_candidates {
					let mut best_supply_info = best_supply_info_ref.borrow_mut();

					let best_supply_partner_interface = self.interface().economy_agent(best_supply_info.economy_agent);

					let span = tracing::trace_span!("trade", partner = best_supply_partner_interface.to_string(), good = TradeableUnit::UNIT_NAME);
					let enter = span.enter();

					tracing::trace!("Considering {} by {}", best_supply_info.supply, best_supply_partner_interface);

					let mut best_demands = || {
						let mut good_ones = None;
						for (effective_max_price_for_supplier, demand_infos) in best_supply_info.demands.iter_mut().rev() {
							demand_infos.retain(|demand_info_ref| {
								let demand_info = demand_info_ref.borrow();
								tracing::trace!(
									"possible demand: {} by {}, bankrupt? {}",
									demand_info.demand,
									self.interface().economy_agent(demand_info.economy_agent),
									bankrupts.contains(&demand_info.economy_agent)
								);
								!bankrupts.contains(&demand_info.economy_agent)
									&& !demand_info.demand.budget.is_some_and(|budget| budget.is_zero())
									&& !demand_info.demand.quantity.is_some_and(|quantity| quantity.is_zero())
							});

							if demand_infos.is_empty() {
								empty_cans.push(*effective_max_price_for_supplier);
							} else if let Some(best_demands) = good_ones {
								// we already found good ones, that's second best for price negotiation
								return Some((best_demands, Some(*effective_max_price_for_supplier)));
							} else {
								good_ones = Some((*effective_max_price_for_supplier, demand_infos.clone()));
							}
						}
						for empty_can in &empty_cans {
							best_supply_info.demands.remove(empty_can);
						}
						good_ones.map(|g_o| (g_o, None))
					};

					let supply_best_demand_set_option = best_demands();

					let ((effective_max_price_for_supplier, supply_best_demand_set), next_effective_max_price_for_supplier_option) = if let Some(candidates) = supply_best_demand_set_option {
						candidates
					} else {
						continue;
					};

					let demands_str: Vec<_> = supply_best_demand_set.iter().map(|d| DemandInfo::to_string(&d.borrow(), self)).collect();
					tracing::trace!(
						"best supply: {}, its best demands for {effective_max_price_for_supplier}: {}, next best demand price: {:?}",
						best_supply_info.to_string(self),
						demands_str.join(","),
						next_effective_max_price_for_supplier_option.map(Rational::floatprint).unwrap_or_else(|| "n/a".to_string())
					);

					// Proceed if supply and demand are best for each other
					if !supply_best_demand_set.contains(&demand_info_ref) {
						continue;
					};

					let negotiated_price = if demand_info_ref.borrow().demand.fixed_price || best_supply_info.supply.fixed_price {
						// if one of the agents does not negotiate, then they already have a price that has been checked
						effective_min_price_for_demander
					} else if let Some(next_effective_min_price_for_demander) = next_effective_min_price_for_demander_option {
						// if there is a supplier competition, we will raise the price to the lowest of the competitor
						next_effective_min_price_for_demander
					} else {
						let upper_bound = effective_max_price_for_supplier;
						let lower_bound = effective_min_price_for_demander;
						tracing::trace!("negotiating between: {}-{}", lower_bound.floatprint(), upper_bound.floatprint());

						upper_bound.add(lower_bound).div(2)
					};

					tracing::trace!("negotiated price: {}, shipping: {}", negotiated_price.floatprint(), shipping_price.floatprint(),);

					let goods_available = best_supply_info.supply.quantity.min(*TradeableUnit::unit_storage(best_supply_info.economy_agent.goods_storage(self)));

					// add this deal to the list of ones to use in this pass
					supply_deals.entry(negotiated_price).or_default().push((goods_available, best_supply_info_ref, shipping_price));
					drop(enter);
				}

				for (negotiated_price, priced_supply_deals) in supply_deals {
					let mut demand_info = demand_info_ref.borrow_mut();
					let demand_energy_credits = demand_info.economy_agent.goods_storage(self).energy_credits;

					let total_goods_available: Unit<TradeableUnit> = priced_supply_deals.iter().map(|d| d.0).sum();
					tracing::trace!("demander price: {}, total goods: {total_goods_available}", negotiated_price.floatprint());

					let goods_budgeted: Unit<TradeableUnit> = if let Some(budget) = demand_info.demand.budget {
						budget.div(negotiated_price).into()
					} else {
						total_goods_available
					};

					tracing::trace!("budget for {}", goods_budgeted);

					let goods_affordable = if demand_info.demand.issues_energy_credits {
						goods_budgeted
					} else {
						goods_budgeted.min(Unit::from_r_floor(demand_energy_credits.div(negotiated_price)))
					};
					assert!(goods_affordable.microunits >= 0);
					if goods_affordable.is_zero() {
						tracing::trace!("Can't afford the best deal, this demander is bankrupt");
						bankrupts.insert(demand_info.economy_agent);
						deals_modified = true;
						continue;
					}

					let goods_to_buy = if let Some(quantity) = demand_info.demand.quantity {
						tracing::trace!("{} demanded", quantity);
						quantity.min(goods_affordable)
					} else {
						goods_affordable
					};

					let goods_part_to_buy: Rational = goods_to_buy.div(total_goods_available).min(1.into());

					tracing::trace!("Can afford {} goods, total: {total_goods_available}", goods_to_buy);

					for (goods_available, best_supply_partner_ref, shipping_price) in priced_supply_deals {
						let mut supply_info = best_supply_partner_ref.borrow_mut();

						// deal amount is minimum 1 microunit to avoid splitting the deal to unbuyable parts. this can probably be handled in more sophisticated way or culled earlier
						let deal_amount_part: Unit<TradeableUnit> = Unit::<TradeableUnit>::from_r_ceil(goods_available.mul(goods_part_to_buy)).max(Unit::from_microunits(1));

						let deal_amount_demanded = if let Some(demand_quantity) = demand_info.demand.quantity {
							deal_amount_part.min(demand_quantity)
						} else {
							deal_amount_part
						};

						let deal_amount = deal_amount_demanded.min(supply_info.supply.quantity);

						// Failed to buy at least 1 mk goods at the best possible price. Out of market.
						let mut bankrupt_me = || {
							bankrupts.insert(demand_info.economy_agent);
							tracing::trace!("bankrupt!");
							deals_modified = true;
						};

						if deal_amount.is_zero() {
							tracing::trace!("deal amount is zero, bankrupt");
							bankrupt_me();
							break;
						};

						let shipping_sum: Unit<EnergyCredit> = deal_amount.mul(shipping_price).into();
						if shipping_sum.is_zero() {
							tracing::trace!("shipping total is too low, bankrupt");
							bankrupt_me();
							break;
						};
						let demander_sum: Unit<EnergyCredit> = Unit::from(deal_amount.mul(negotiated_price)).max(Unit::from_microunits(1));
						let supplier_sum = demander_sum.sub(shipping_sum).max(Unit::from_microunits(1));

						let demand_energy_credits = demand_info.economy_agent.goods_storage(self).energy_credits;

						if demand_info.demand.budget.is_some_and(|budget| budget < demander_sum) {
							tracing::trace!("budget too small, bankrupt");
							bankrupt_me();
							break;
						};

						if !demand_info.demand.issues_energy_credits && demand_energy_credits < demander_sum {
							tracing::trace!("not enough credits, bankrupt");
							bankrupt_me();
							break;
						};

						tracing::trace!("deal is ok: can afford {} for {}(shipping: {})", deal_amount, demander_sum, shipping_sum);

						deals += 1;

						// remove goods
						TradeableUnit::unit_storage(supply_info.economy_agent.goods_storage(self)).sub_assign(deal_amount);
						assert!(TradeableUnit::unit_storage(supply_info.economy_agent.goods_storage(self)).microunits >= 0);

						// reduce supply
						supply_info.supply.quantity.sub_assign(deal_amount);
						assert!(supply_info.supply.quantity.microunits >= 0);

						// add income
						if !supply_info.supply.issues_energy_credits {
							mailboxes.entry(supply_info.economy_agent).or_default().energy_credits.add_assign(supplier_sum);
						};

						// add goods
						TradeableUnit::unit_storage(mailboxes.entry(demand_info.economy_agent).or_default()).add_assign(deal_amount);

						// reduce demand
						if let Some(demand_quantity) = &mut demand_info.demand.quantity {
							demand_quantity.sub_assign(deal_amount);
							assert!(demand_quantity.microunits >= 0);
						}

						let economy_agent = demand_info.economy_agent;

						// remove payment
						if !demand_info.demand.issues_energy_credits {
							economy_agent.pay(demander_sum, self);
							assert!(economy_agent.goods_storage(self).energy_credits.microunits >= 0);
						};

						// reduce budget
						if let Some(budget) = &mut demand_info.demand.budget {
							budget.sub_assign(demander_sum);
							assert!(budget.microunits >= 0);
						}

						let trade_transaction = TradeTransaction {
							source_economy_agent: supply_info.economy_agent,
							target_economy_agent: demand_info.economy_agent,
							cargo: TradeableUnit::to_cargo(deal_amount),
							// paid to transportation
							shipping_fee: shipping_sum,
							// paid to supplier
							income: supplier_sum,
							turn_number: self.turn_number,
							id: ID::invalid(),
						};
						let _trade_transaction_id = self.insert(trade_transaction)?;
					}
				}
				drop(demand_enter);
			}

			tracing::trace!("Deals made during an iteration: {deals}{}", if deals_modified { " (modified)" } else { "" });
			if deals == 0 && !deals_modified {
				break;
			};
			loop_number.add_assign(1);
		}
		drop(enter);
		Ok(())
	}

	pub fn trade(&mut self) -> Result<()> {
		let interface = self.interface();

		let mut food_traded_good_datas: Vec<_> = Default::default();
		let mut fuel_traded_good_datas: Vec<_> = Default::default();

		let mut mailboxes: HashMap<EconomyAgent, GoodsStorage> = Default::default();

		for economy_agent in interface
			.planets()
			.iter()
			.map(|v| EconomyAgent::Planet(v.data.id))
			.chain(interface.fleets().iter().map(|v| EconomyAgent::Fleet(v.data.id)))
			.chain(
				interface
					.empires()
					.iter()
					.filter_map(|e| e.capital().map(|c| (e, c)))
					.map(|(empire, capital)| EconomyAgent::VoidChurchBranch(empire.data.id, capital.data.id)),
			) {
			let tgd = interface.economy_agent(economy_agent).trading_data();
			food_traded_good_datas.push((economy_agent, tgd.food));
			fuel_traded_good_datas.push((economy_agent, tgd.fuel));
		}

		self.perform_trade::<Food>(food_traded_good_datas, &mut mailboxes)?;
		self.perform_trade::<Fuel>(fuel_traded_good_datas, &mut mailboxes)?;

		for (economy_agent, mut goods_storage) in mailboxes {
			economy_agent.pay_taxes(&mut goods_storage.energy_credits, self);
			economy_agent.goods_storage(self).add_assign(goods_storage);
		}

		Ok(())
	}

	/// Migrate population between planets
	pub fn migrate(&mut self) {
		// ec/human/breach
		let migration_cost = Rational::from(10);
		let min_migration_budget = 1000.into();
		let min_migration_batch = Unit::from(100);
		let max_migration_population_rate = Rational::from((1, 10));
		let min_migration_population = Unit::from(2000);

		let ui = self.interface();
		let star_system_ids_planet_ids: Vec<(ID<StarSystem>, Vec<ID<Planet>>)> = ui.star_systems().into_iter().map(|s| (s.data.id, s.planets().into_iter().map(|p| p.data.id).collect())).collect();
		for (star_system_id, planet_ids) in star_system_ids_planet_ids.iter() {
			for planet_id in planet_ids {
				let ui = self.interface();
				let planet = ui.planet(*planet_id);
				let budget = planet.data.goods_storage.energy_credits;
				let population_available: Unit<Population> = planet.data.population.mul(max_migration_population_rate).into();

				tracing::trace!("Migrating {planet}: {population_available} {budget} ins: {}", planet.insurance_cost());

				if planet.data.population < min_migration_population {
					tracing::trace!("Population too low");
					continue;
				};
				// skip if the planet too poor to consider migration
				if planet.data.goods_storage.energy_credits < min_migration_budget {
					tracing::trace!("Budget is too low");
					continue;
				};

				let mut migration_targets: BTreeMap<Rational, BTreeMap<Rational, Vec<ID<Planet>>>> = Default::default();

				for (target_star_system_id, _planet_ids) in star_system_ids_planet_ids.iter() {
					let ui = self.interface();
					let target_star_system = ui.star_system(*target_star_system_id);
					let star_system = ui.star_system(*star_system_id);
					let planet = ui.planet(*planet_id);
					tracing::trace!("trying {target_star_system}, ins: {}", planet.insurance_cost());
					let good_migration_target = target_star_system
						.planets()
						.iter()
						.map(|target_planet| target_planet.insurance_cost() < planet.insurance_cost())
						.any(|b| b);
					if !good_migration_target {
						tracing::trace!("No good targets");
						continue;
					};
					let (_path, cost) = if let Some(pcost) = star_system.shipping_path_cost(target_star_system, star_system.empire()) {
						pcost
					} else {
						tracing::trace!("No path");
						continue;
					};

					for target_planet in target_star_system.planets().iter() {
						let insurance_difference = target_planet.insurance_cost() - planet.insurance_cost();
						tracing::trace!("{target_planet} insurance diff: {insurance_difference}");
						if insurance_difference >= Rational::from(0) {
							continue;
						};
						let travel_cost = cost * migration_cost;
						let profit_expectance = travel_cost / insurance_difference;

						migration_targets.entry(profit_expectance).or_default().entry(travel_cost).or_default().push(target_planet.data.id);
					}
				}

				for (_profit_expectance, priced_target_planet_ids) in migration_targets.into_iter().rev() {
					let ui = self.interface();
					let planet = ui.planet(*planet_id);

					if !(planet.data.goods_storage.energy_credits.microunits > 0 && population_available.microunits > 0) {
						break;
					}

					for (travel_cost, target_planet_ids) in priced_target_planet_ids {
						for target_planet_id in target_planet_ids {
							if target_planet_id == *planet_id {
								continue;
							};
							let ui = self.interface();
							let planet = ui.planet(*planet_id);
							let target_planet = ui.planet(target_planet_id);

							let population_affordable = if travel_cost.is_zero() {
								planet.data.population
							} else {
								planet.data.goods_storage.energy_credits.div(travel_cost).floor().into()
							};

							tracing::trace!(
								"affordable for {} * {}: {}",
								planet.data.goods_storage.energy_credits.microunits,
								travel_cost,
								population_affordable.microunits
							);

							if population_affordable.microunits == 0 {
								break;
							};

							let population_accomodable = planet.data.population.mul(2).sub(&target_planet.data.population).max(0.into());

							let migrating_to_this = population_accomodable.min(population_affordable).min(planet.data.population);

							tracing::trace!("migrating: {}", migrating_to_this.microunits);

							if migrating_to_this > min_migration_batch {
								let migration_payment: Unit<EnergyCredit> = (migrating_to_this.mul(travel_cost)).into();
								tracing::trace!("payment: {}", migration_payment.microunits);

								let source_planet_id = planet.data.id;
								let target_planet_id = target_planet.data.id;

								let source_planet = self.getf_mut(source_planet_id).unwrap();
								source_planet.population = source_planet.population.sub(migrating_to_this);
								assert!(source_planet.population.microunits >= 0);
								source_planet.goods_storage.energy_credits = source_planet.goods_storage.energy_credits.sub(migration_payment);
								assert!(source_planet.goods_storage.energy_credits.microunits >= 0);
								let target_planet = self.getf_mut(target_planet_id).unwrap();
								target_planet.population = target_planet.population.add(migrating_to_this);
								self.migration_events.push(MigrationEvent {
									source_planet_id,
									target_planet_id,
									fee: migration_payment,
									population: migrating_to_this,
								});
							};
						}
					}
				}
			}
		}
	}

	/// produce goods, consume upkeep
	pub fn produce(&mut self) {
		let ui = self.interface();
		let mut planets_updates = vec![];

		// we consider planets can't be outside of star systems
		for star_system in ui.star_systems() {
			for planet in star_system.planets() {
				let span = tracing::debug_span!("production", planet = planet.to_string());
				let enter = span.enter();
				tracing::trace!("Production at planet {planet}");

				let private_energy_credits = planet.data.goods_storage.energy_credits;

				let mut private_food = planet.data.goods_storage.food;
				let mut private_fuel = planet.data.goods_storage.fuel;

				let mut economic_asset_list = planet.data.economic_asset_list.clone();
				tracing::trace!("fuel: {private_fuel}, pop: {}, food: {}", planet.data.population, planet.data.goods_storage.food);

				let insurable_pop: Unit<Population> = private_fuel.div(planet.insurance_cost()).into();
				let feedable_pop = private_food.div(planet.food_consumption()).into();
				let mut workforce = insurable_pop.min(feedable_pop).min(planet.data.population);

				tracing::trace!("can insure {}, can feed {}", insurable_pop, feedable_pop);

				let nils = planet.data.population.sub(workforce);

				let reproduced_workforce: Unit<Population> = Unit::from_r_ceil(Rational::from(workforce).mul(Rational::from(1) + Rational::from((4, 10000))));
				tracing::trace!("reproduced to {reproduced_workforce} + {nils}");
				let reproduced_population = reproduced_workforce + nils;

				private_fuel -= workforce.mul(planet.insurance_cost()).into();
				tracing::trace!("after workforce: {private_fuel}");
				private_food -= workforce.cast();

				let food_needed = planet.food_to_produce().sub(private_food).max(0.into());
				let (farmers_needed, maintenance_needed): (Unit<Population>, Unit<Fuel>) =
					(food_needed.mul(Farm::workforce_cost(planet)).into(), food_needed.mul(Farm::maintenance_cost(planet)).into());

				tracing::trace!(
					"Food needed: {food_needed}. Farmers needed: {farmers_needed}, cost: {maintenance_needed}, farms present: {}",
					economic_asset_list.farms
				);

				let max_farmer_ratio = if farmers_needed.is_zero() { 0.into() } else { farmers_needed.min(workforce).div(farmers_needed) };
				let max_fuel_ratio = if maintenance_needed.is_zero() {
					0.into()
				} else {
					maintenance_needed.min(private_fuel).div(maintenance_needed)
				};
				let max_asset_ratio = if food_needed.is_zero() {
					0.into()
				} else {
					food_needed.min(economic_asset_list.farms.cast()).div(food_needed)
				};

				let farm_ratio = max_fuel_ratio.min(max_farmer_ratio).min(max_asset_ratio);

				let farmers = if farm_ratio > Rational::from(0) {
					// operate some farms

					let fuel_price: Rational = farm_ratio.mul(maintenance_needed);
					let workforce_price = farm_ratio.mul(farmers_needed);
					private_fuel.sub_assign(fuel_price.into());
					workforce.sub_assign(workforce_price.into());
					private_food.add_assign(food_needed.mul(farm_ratio).into());
					Unit::from(workforce_price)
				} else {
					0.into()
				};

				// man the sifters if workforce is left
				let active_sifters: Unit<FuelSifter> = Rational::from(economic_asset_list.fuel_sifters)
					.min(Rational::from(workforce).div(FuelSifter::workforce_cost(planet)))
					.into();
				private_fuel.add_assign(active_sifters.cast());
				let ec_gatherers = active_sifters.mul(FuelSifter::workforce_cost(planet)).into();
				workforce.sub_assign(ec_gatherers);

				tracing::trace!("{} active", active_sifters);

				// if some spare fuel is left, construct things

				let construction_fuel = private_fuel.sub(planet.fuel_to_produce()).max(Unit::zero());

				// TEMP: calculate ROI considering 1 food = 1 fuel

				let farm_sifter_ratio = FuelSifter::construction_cost(planet).div(Farm::construction_cost(planet));

				let sifters = Rational::from(construction_fuel).div(FuelSifter::construction_cost(planet).add(farm_sifter_ratio.mul(Farm::construction_cost(planet))));
				let farms = sifters.mul(farm_sifter_ratio);

				tracing::trace!("Got {} fuel, constructing {} {}", construction_fuel, sifters.floatprint(), farms.floatprint());

				// construct more sifters if some disposable fuel is left

				let sifter_units = Unit::from_r_floor(sifters);
				economic_asset_list.fuel_sifters.add_assign(sifter_units);
				private_fuel.sub_assign(Rational::from(sifter_units).mul(FuelSifter::construction_cost(planet)).into());

				let farm_units = Unit::from_r_floor(farms);
				economic_asset_list.farms.add_assign(farm_units);
				private_fuel.sub_assign(Rational::from(farm_units).mul(Farm::construction_cost(planet)).into());

				// NILs will do inefficient farming and EC gathering
				private_fuel.add_assign(nils.mul(Rational::from((125, 1000))).mul(planet.data.fuel_availability).into());
				private_food.add_assign(nils.mul(Rational::from((1, 4))).into());

				planets_updates.push((planet.data.id, move |planet: &mut Planet| {
					planet.population = reproduced_population;
					planet.goods_storage.energy_credits = private_energy_credits;
					planet.goods_storage.food = private_food;
					planet.goods_storage.fuel = private_fuel;
					planet.planetary_economics.nils = nils;
					planet.planetary_economics.farmers = farmers;
					planet.planetary_economics.ec_gatherers = ec_gatherers;
					planet.economic_asset_list = economic_asset_list;
				}));
				drop(enter);
			}
		}

		for (id, update) in planets_updates.into_iter() {
			let planet = self.getf_mut(id).unwrap();
			update(planet);
		}
	}

	pub fn set_empire_capital(&mut self, empire_id: ID<Empire>, star_system_id: ID<StarSystem>) {
		self.capitals_in_empires.add_link(empire_id, star_system_id);
	}

	pub fn resolve_battles(&mut self) {
		let ui = self.interface();

		// FIXME persistent RNG
		let mut rng = rand::thread_rng();

		let mut dead_fleets: HashSet<ID<Fleet>> = Default::default();
		let mut dead_ships = HashSet::new();
		for star_system in ui.star_systems() {
			let mut empires_ships: HashMap<interface::Empire, u64> = HashMap::new();

			for fleet in star_system.fleets() {
				let ships = fleet.ships().len() as u64;
				let empire = fleet.empire();
				empires_ships.entry(empire).and_modify(|e| *e = e.checked_add(ships).unwrap()).or_insert(ships);
			}
			for empire in empires_ships.keys() {
				let mut enemy_ships: Vec<_> = star_system
					.fleets()
					.into_iter()
					.flat_map(|f| f.ships().into_iter())
					.filter(|ship| ship.fleet().empire() != *empire && !dead_ships.contains(ship))
					.collect();

				if !enemy_ships.is_empty() {
					let our_ships = empires_ships.get(empire).unwrap_or(&0);
					for _ in 0..*our_ships {
						// if there is a ship left
						if enemy_ships.is_empty() {
							break;
						};
						// our ship shoots at enemies!
						let which_enemy = rng.gen_range(0..enemy_ships.len());
						// enemy ded, queue for removal
						dead_ships.insert(enemy_ships[which_enemy]);
						// and don't allow further shoots at it
						enemy_ships.remove(which_enemy);
					}
				}
			}
		}
		let dead_ship_ids: Vec<_> = dead_ships.into_iter().map(|ship| ship.data.id).collect();

		for ship in dead_ship_ids {
			self.remove(ship).unwrap();
		}

		for fleet in self.interface().fleets() {
			if fleet.ships().is_empty() {
				dead_fleets.insert(fleet.data.id);
			}
		}

		for dead_fleet_id in dead_fleets {
			self.remove(dead_fleet_id).unwrap();
		}
	}

	pub fn check_winner(&mut self) {
		let ui = self.interface();
		let capital_holders: Vec<_> = ui
			.empires()
			.iter()
			.map(|empire| {
				let held = match empire.capital() {
					None => false,
					Some(capital) => capital.empire() == Some(*empire),
				};
				(held, *empire)
			})
			.filter(|(held, _)| *held)
			.collect();
		self.win_state = match capital_holders.len() {
			0 => WinState::Draw,
			1 => WinState::Winner(capital_holders[0].1.data.id),
			_ => WinState::NoWinner,
		}
	}

	pub fn empire_view(&self, empire_id: ID<Empire>) -> crate::universeview::Universe {
		crate::universeview::Universe::empire_view(self, empire_id)
	}

	pub fn calculate_trading_parents(&mut self) {
		let interface = self.interface();
		let mut trading_parents_maps: TradingParentsMaps = Default::default();

		for empire in interface.empires().into_iter().map(Some).chain([None].into_iter()) {
			let mut trading_parents_map: TradingParentsMap = Default::default();

			for star_system1 in interface.star_systems() {
				let parents = pathfinding::directed::dijkstra::dijkstra_all(&star_system1.data.id, |star_system_id| {
					let star_system = interface.star_system(*star_system_id);
					let vec: Vec<_> = star_system
						.starlanes()
						.into_iter()
						.map(|linked_star_system| (linked_star_system.data.id, star_system.breach_shipping_cost(linked_star_system, empire).unwrap()))
						.collect();
					vec
				});
				trading_parents_map.insert(star_system1.data.id, parents);
			}
			trading_parents_maps.insert(empire.map(|e| e.data.id), trading_parents_map);
		}
		self.trading_parents_maps = trading_parents_maps;
	}
}

pub trait Transportable: UnitType {
	fn cost_per_unit() -> Rational;
	// we have to use these because of limits on where bounds
	fn into_cargo(units: Unit<Self>) -> Cargo;
	fn tryfrom_cargo(cargo: Cargo) -> Option<Unit<Self>>;
}

impl Transportable for Food {
	fn cost_per_unit() -> Rational {
		(1, 1_000).into()
	}

	fn into_cargo(units: Unit<Self>) -> Cargo {
		Cargo::Food(units)
	}

	fn tryfrom_cargo(cargo: Cargo) -> Option<Unit<Self>> {
		if let Cargo::Food(units) = cargo {
			Some(units)
		} else {
			None
		}
	}
}

impl Transportable for Fuel {
	fn cost_per_unit() -> Rational {
		(1, 100).into()
	}

	fn into_cargo(units: Unit<Self>) -> Cargo {
		Cargo::Fuel(units)
	}

	fn tryfrom_cargo(cargo: Cargo) -> Option<Unit<Self>> {
		if let Cargo::Fuel(units) = cargo {
			Some(units)
		} else {
			None
		}
	}
}

impl<TransportableUnit: Transportable> From<Unit<TransportableUnit>> for Cargo {
	fn from(u: Unit<TransportableUnit>) -> Self {
		Transportable::into_cargo(u)
	}
}

impl<TransportableUnit: Transportable> TryFrom<Cargo> for Unit<TransportableUnit> {
	type Error = ();

	fn try_from(value: Cargo) -> std::result::Result<Self, Self::Error> {
		Transportable::tryfrom_cargo(value).ok_or(())
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Default)]
pub struct PlanetaryEconomics {
	pub nils: Unit<Population>,
	pub farmers: Unit<Population>,
	pub ec_gatherers: Unit<Population>,
}

#[derive(Clone, Hash, PartialEq, Eq, Copy, Debug, Serialize, Deserialize)]
pub enum EconomyAgent {
	Planet(ID<Planet>),
	Fleet(ID<Fleet>),
	Government(ID<Empire>),
	VoidChurchBranch(ID<Empire>, ID<StarSystem>),
}

impl EconomyAgent {
	fn goods_storage(self, universe: &mut Universe) -> &mut GoodsStorage {
		match self {
			EconomyAgent::Planet(id) => {
				let stored = universe.getf_mut(id).unwrap();
				&mut stored.goods_storage
			}
			EconomyAgent::Fleet(id) => {
				let stored = universe.getf_mut(id).unwrap();
				&mut stored.goods_storage
			}
			EconomyAgent::Government(id) => {
				let stored = universe.getf_mut(id).unwrap();
				&mut stored.goods_storage
			}
			EconomyAgent::VoidChurchBranch(_, _) => &mut universe.void_church.goods_storage,
		}
	}

	fn pay(self, gross_pay: Unit<EnergyCredit>, universe: &mut Universe) {
		match self {
			EconomyAgent::Fleet(_id) => {
				// (for now?) governments pay for fleets
				let interface = universe.interface().economy_agent(self);
				let empire_id_option = interface.empire().map(|e| e.data.id);
				if let Some(empire_id) = empire_id_option {
					let empire = universe.getf_mut(empire_id).unwrap();
					empire.goods_storage.energy_credits.sub_assign(gross_pay);
				} else {
					self.goods_storage(universe).energy_credits.sub_assign(gross_pay);
				}
			}
			_ => {
				self.goods_storage(universe).energy_credits.sub_assign(gross_pay);
			}
		}
	}

	/// Add EC income, paying taxes if applicable
	fn pay_taxes(self, gross_pay: &mut Unit<EnergyCredit>, universe: &mut Universe) {
		let interface = universe.interface().economy_agent(self);
		let tax_data = if let interface::EconomyAgent::Planet(planet) = &interface { planet.income_tax() } else { None };
		if let Some(tax_rate) = tax_data {
			let tax: Unit<EnergyCredit> = Rational::from(*gross_pay).mul(tax_rate).into();
			let net_pay = gross_pay.sub(tax);
			tracing::trace!("Paying {tax} tax from {gross_pay}");
			*gross_pay = net_pay;
		}
	}
}

#[derive(Clone, PartialEq, Default, Debug)]
pub struct TradingData {
	pub food: TradedGoodData<Food>,
	pub fuel: TradedGoodData<Fuel>,
}

#[derive(Clone, PartialEq, Eq, Default, Serialize, Deserialize, Debug)]
pub struct GoodsStorage {
	pub food: Unit<Food>,
	pub energy_credits: Unit<EnergyCredit>,
	pub fuel: Unit<Fuel>,
}

impl Add<GoodsStorage> for &GoodsStorage {
	type Output = GoodsStorage;
	fn add(self, other: GoodsStorage) -> GoodsStorage {
		GoodsStorage {
			food: self.food + other.food,
			energy_credits: self.energy_credits + other.energy_credits,
			fuel: self.fuel + other.fuel,
		}
	}
}

impl AddAssign<GoodsStorage> for GoodsStorage {
	fn add_assign(&mut self, rhs: Self) {
		*self = self.add(rhs)
	}
}

impl std::iter::Sum for GoodsStorage {
	fn sum<I: Iterator<Item = GoodsStorage>>(iter: I) -> GoodsStorage {
		iter.fold(Default::default(), |a, b| a.add(b))
	}
}

pub trait Tradeable: UnitType + Transportable {
	fn unit_storage(goods_storage: &mut GoodsStorage) -> &mut Unit<Self>;
	fn to_cargo(unit: Unit<Self>) -> Cargo;
}

impl Tradeable for Food {
	fn unit_storage(goods_storage: &mut GoodsStorage) -> &mut Unit<Self> {
		&mut goods_storage.food
	}
	fn to_cargo(unit: Unit<Self>) -> Cargo {
		Cargo::Food(unit)
	}
}

impl Tradeable for Fuel {
	fn unit_storage(goods_storage: &mut GoodsStorage) -> &mut Unit<Self> {
		&mut goods_storage.fuel
	}

	fn to_cargo(unit: Unit<Self>) -> Cargo {
		Cargo::Fuel(unit)
	}
}

/// Information about how much goods of this type the trading partner wants to trade and at what price
#[derive(Clone, PartialEq, Debug, Default)]
pub struct TradedGoodData<TradedGood: UnitType> {
	pub demands: Vec<Demand<TradedGood>>,
	pub supplies: Vec<Supply<TradedGood>>,
}

impl<TradedGood: UnitType> std::fmt::Display for TradedGoodData<TradedGood> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Demands: ")?;
		for demand in &self.demands {
			demand.fmt(f)?;
		}
		write!(f, ", supplies: ")?;
		for supply in &self.supplies {
			supply.fmt(f)?;
		}
		Ok(())
	}
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Default)]
pub struct Demand<TradedGood: UnitType> {
	/// How much the partner wants to buy. None = infinite.
	pub quantity: Option<Unit<TradedGood>>,
	pub max_price: Rational,
	/// Always pay the max price
	pub fixed_price: bool,
	/// Issue new EC instead of paying them
	pub issues_energy_credits: bool,
	pub will_not_pay_shipping: bool,
	/// Buyer's allocated budget. None = infinite
	pub budget: Option<Unit<EnergyCredit>>,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Default)]
pub struct Supply<TradedGood: UnitType> {
	/// How much there is to sell.
	pub quantity: Unit<TradedGood>,
	/// Always pay the minimum price
	pub fixed_price: bool,
	/// This supply destroys EC instead of receiving them
	pub issues_energy_credits: bool,
	/// Will not pay shipping, if demand is refusing
	pub will_not_pay_shipping: bool,
	/// Will pay for shipping without increasing price. Mainly useful for disposing of fuel while not going lower than VC, probably. Temporary measure? Should have real price monitoring and estimation, instead
	pub shipping_included: bool,
	pub min_price: Rational,
}

// this really shouldn't be a thing but for matching it gotta be a thing. Maybe it should have IDs...

impl<'a, TradeableUnit: Tradeable> PartialEq for SupplyInfo<'a, TradeableUnit> {
	fn eq(&self, other: &Self) -> bool {
		self.supply.eq(&other.supply) && self.economy_agent.eq(&other.economy_agent)
	}
}

impl<'a, TradeableUnit: Tradeable> Eq for SupplyInfo<'a, TradeableUnit> {}

impl<'a, TradeableUnit: Tradeable> PartialEq for DemandInfo<'a, TradeableUnit> {
	fn eq(&self, other: &Self) -> bool {
		self.demand.eq(&other.demand) && self.economy_agent.eq(&other.economy_agent)
	}
}

impl<'a, TradeableUnit: Tradeable> Eq for DemandInfo<'a, TradeableUnit> {}

struct SupplyInfo<'a, TradeableUnit: Tradeable> {
	supply: Supply<TradeableUnit>,
	#[allow(clippy::type_complexity)]
	demands: BTreeMap<Rational, Vec<&'a RefCell<DemandInfo<'a, TradeableUnit>>>>,
	economy_agent: EconomyAgent,
}

impl<'a, TradeableUnit: Tradeable> SupplyInfo<'a, TradeableUnit> {
	fn to_string(&self, u: &Universe) -> String {
		format!("Supply by {} for {}", u.interface().economy_agent(self.economy_agent), self.supply)
	}
}

struct DemandInfo<'a, TradeableUnit: Tradeable> {
	demand: Demand<TradeableUnit>,
	#[allow(clippy::type_complexity)]
	supplies: BTreeMap<Rational, Vec<(&'a RefCell<SupplyInfo<'a, TradeableUnit>>, Rational)>>,
	economy_agent: EconomyAgent,
}

impl<'a, TradeableUnit: Tradeable> DemandInfo<'a, TradeableUnit> {
	fn to_string(&self, u: &Universe) -> String {
		format!("Demand by {} for {}", u.interface().economy_agent(self.economy_agent), self.demand)
	}
}

impl<TradeableUnit: UnitType> std::fmt::Display for Supply<TradeableUnit> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}", self.quantity)?;
		if self.fixed_price {
			write!(f, " at {}", self.min_price.floatprint())?;
		} else {
			write!(f, " min {}", self.min_price.floatprint())?;
		};
		if self.shipping_included {
			write!(f, " plus shipping")?;
		};
		if self.issues_energy_credits {
			write!(f, ", issuer")?;
		};
		Ok(())
	}
}

impl<TradeableUnit: UnitType> std::fmt::Display for Demand<TradeableUnit> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		if let Some(quantity) = self.quantity {
			write!(f, "{}", quantity)?;
		} else {
			write!(f, "inf {}", TradeableUnit::UNIT_NAME)?;
		};
		if self.fixed_price {
			write!(f, " at {}", self.max_price.floatprint())?;
		} else {
			write!(f, " max {}", self.max_price.floatprint())?;
		};
		if self.issues_energy_credits {
			write!(f, " issued")?;
		};
		if let Some(budget) = self.budget {
			write!(f, " budget {}", budget)?;
		};
		Ok(())
	}
}

#[derive(Clone, Serialize, Deserialize, Default, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub struct EconomicAssetList {
	pub farms: Unit<Farm>,
	pub fuel_sifters: Unit<FuelSifter>,
}

pub trait EconomicAsset {
	/// EC needed to construct 1 asset on a given planet
	fn construction_cost(planet: interface::Planet) -> Rational;
	/// EC needed to maintain 1 asset for 1 week on a given planet
	fn maintenance_cost(planet: interface::Planet) -> Rational;
	/// Humans (1 milliunit) needed to work the asset
	fn workforce_cost(planet: interface::Planet) -> Rational;
}

impl EconomicAsset for FuelSifter {
	fn construction_cost(planet: interface::Planet) -> Rational {
		// fuel sifters directly scale on local fuel availability and are not significantly influenced by other factors

		// basic shaping and furniture
		let base_cost = Rational::from(50);

		let foundation = Rational::from(10).mul(planet.surface_bio_quality_ediff());
		let atmo = Rational::from(10).mul(planet.atmosphere_bio_quality_ediff());
		let cosmic_ray = Rational::from(10).mul(planet.effective_cosmic_ray_intensity());

		let mining_equipment = Rational::from(50).mul(Rational::one().sub(planet.data.fuel_availability));

		base_cost.add(foundation).add(atmo).add(cosmic_ray).add(mining_equipment)
	}
	fn maintenance_cost(_planet: interface::Planet) -> Rational {
		0.into() // pays for itself
	}
	fn workforce_cost(planet: interface::Planet) -> Rational {
		Rational::one().div(planet.data.fuel_availability)
	}
}

impl EconomicAsset for Farm {
	fn construction_cost(planet: interface::Planet) -> Rational {
		// farming assets are cheaper and more affected by surface bio quality and less by atmo and cosmic rays

		// basic shaping and furniture
		let base_cost = Rational::from(50);

		let foundation = Rational::from(50).mul(planet.surface_bio_quality_ediff());
		let atmo = Rational::from(10).mul(planet.atmosphere_bio_quality_ediff());

		let cosmic_ray = Rational::from(10).mul(planet.effective_cosmic_ray_intensity());
		base_cost.add(foundation).add(atmo).add(cosmic_ray)
	}
	fn maintenance_cost(planet: interface::Planet) -> Rational {
		Rational::from((1, 10))
			+ Rational::from((125, 1000)) * (Rational::from(1).sub(Rational::from(planet.data.atmosphere_bio_quality)))
			+ Rational::from(2) * (Rational::from(1).sub(Rational::from(planet.data.surface_bio_quality)))
	}
	fn workforce_cost(_planet: interface::Planet) -> Rational {
		(1, 4).into()
	}
}

#[cfg(test)]
mod tests {
	// Note this useful idiom: importing names from outer (for mod tests) scope.
	use super::*;
	use crate::universegen;
	/// Two empires engage in a reckless assault, strategic acquirement and victorious counteroffensive.
	// TODO: review this for usefulness and interface usage
	#[test]
	fn test_galactic_war() -> anyhow::Result<()> {
		crate::ensure_tracing_subscriber();
		let params = universegen::UniverseParameters {
			star_systems_per_empire: 10,
			empires_n: 2,
			add_testables: true,
			generator: universegen::UniverseGenerator::Haphazard,
			fixed_population: Some(1e9 as u64),
			molotov_ribbentrop: false,
			seed: Some(0),
			..Default::default()
		};

		let mut u = universegen::generate_universe(&params)?;
		let ship_cost = u.ship_cost;

		let empire_1_id = u.empires.iter().find(|e| e.1.name == "Empire1").unwrap().1.id;
		let e1 = action::Actor { empire_id: empire_1_id.cast() };
		let empire_2_id = u.empires.iter().find(|e| e.1.name == "Empire2").unwrap().1.id;
		let e2 = action::Actor { empire_id: empire_2_id.cast() };

		let capital1: ID<crate::universeview::StarSystem> = u.star_systems.iter().find(|s| s.1.name == "StarSystem1").unwrap().1.id.cast();
		let capital2: ID<crate::universeview::StarSystem> = u.star_systems.iter().find(|s| s.1.name == "StarSystem2").unwrap().1.id.cast();

		u.getf_mut(empire_1_id)?.goods_storage.energy_credits = ship_cost * 2;
		u.getf_mut(empire_2_id)?.goods_storage.energy_credits = ship_cost * 2;

		let fleet1_action = action::Action::BuildFleets {
			star_system_id: capital1.cast(),
			fleets_number: 1,
		};

		let fleet2_action = action::Action::BuildFleets {
			star_system_id: capital2.cast(),
			fleets_number: 1,
		};

		assert!(u.set_actions(Some(vec![fleet1_action]), &e1).is_ok());
		assert!(u.set_actions(Some(vec![fleet2_action]), &e2).is_ok());

		u.make_turn()?;

		let ui = u.interface();
		let fleet1 = ui.empire(empire_1_id).fleets()[0].data.id;
		let fleet2 = ui.empire(empire_2_id).fleets()[0].data.id;

		assert_eq!(ui.fleet(fleet1).star_system().data.id, capital1.cast());
		assert_eq!(ui.fleet(fleet2).star_system().data.id, capital2.cast());

		u.getf_mut(empire_1_id)?.goods_storage.energy_credits = ship_cost * 7;

		let bad_actions = vec![action::Action::BuildShips {
			fleet_id: fleet1.cast(),
			ships_number: 8,
		}];

		// a bit too many ships
		assert!(u.set_actions(Some(bad_actions), &e1).is_err());

		let actions = vec![action::Action::BuildShips {
			fleet_id: fleet1.cast(),
			ships_number: 7,
		}];

		u.set_actions(Some(actions), &e1)?;

		u.getf_mut(empire_2_id)?.goods_storage.energy_credits = ship_cost * 8;

		let actions = vec![action::Action::BuildShips {
			fleet_id: fleet2.cast(),
			ships_number: 8,
		}];

		u.set_actions(Some(actions), &e2)?;

		u.make_turn()?;

		let uv1 = u.empire_view(e1.empire_id.cast());

		assert_eq!(8, uv1.ships_in_fleets.children(fleet1.cast()).len());
		assert_eq!(9, uv1.ships_in_fleets.children(fleet2.cast()).len());

		// Attack capital 2 with fleet of empire 1

		let action = action::Action::MoveFleet {
			fleet_id: fleet1.cast(),
			target_star_system_id: capital2.cast(),
		};
		u.set_actions(Some(vec![action]), &e1)?;

		u.make_turn()?;

		let uv1 = u.empire_view(e1.empire_id.cast());

		// fleet 1 should be dead
		assert_eq!(uv1.get(fleet1.cast()), Option::<&crate::universeview::Fleet>::None);

		// create another one

		u.getf_mut(empire_1_id)?.goods_storage.energy_credits = ship_cost * 2;
		let fleet1_action = action::Action::BuildFleets {
			star_system_id: capital1.cast(),
			fleets_number: 1,
		};
		u.set_actions(Some(vec![fleet1_action]), &e1)?;

		u.make_turn()?;

		let fleet1 = u.interface().empire(empire_1_id).fleets()[0].data.id;

		u.getf_mut(empire_1_id)?.goods_storage.energy_credits = ship_cost * 16;
		// now empire2 should have a sizeable advantage
		let actions = vec![action::Action::BuildShips {
			fleet_id: fleet1.cast(),
			ships_number: 16,
		}];

		u.set_actions(Some(actions), &e1)?;
		u.getf_mut(empire_2_id)?.goods_storage.energy_credits = ship_cost * 20;
		let actions = vec![action::Action::BuildShips {
			fleet_id: fleet2.cast(),
			ships_number: 20,
		}];

		u.set_actions(Some(actions), &e2)?;

		u.make_turn()?;

		let uv2 = u.empire_view(e2.empire_id.cast());

		assert_eq!(17, uv2.ships_in_fleets.children(fleet1.cast()).len());
		assert_eq!(21, uv2.ships_in_fleets.children(fleet2.cast()).len());

		// empire2 launches a counteroffensive
		let action = action::Action::MoveFleet {
			fleet_id: fleet2.cast(),
			target_star_system_id: capital1.cast(),
		};

		u.set_actions(Some(vec![action]), &e2)?;

		u.make_turn()?;

		// e2 wins with advantage
		assert_eq!(u.get(fleet1.cast()), Option::<&crate::universe::Fleet>::None);
		assert_eq!(u.interface().fleet(fleet2.cast()).ships().len(), 4);
		// no winner at this time
		assert_eq!(u.win_state, super::WinState::NoWinner);

		u.getf_mut(empire_2_id)?.goods_storage.energy_credits = u.interface().star_system(capital1.cast()).capture_price();
		// and captures Capital 1
		let actions = vec![action::Action::CaptureStarSystem {
			empire_id: e2.empire_id.cast(),
			star_system_id: capital1,
		}];

		u.set_actions(Some(actions), &e2)?;

		u.make_turn()?;

		// e2 wins
		assert_eq!(u.win_state, super::WinState::Winner(e2.empire_id.cast()));

		// meanwhile, a mysterious rebellion at e2 capital
		u.capitals_in_empires.del_by_parent(e2.empire_id.cast());

		u.make_turn()?;

		// now it's a draw
		assert_eq!(u.win_state, super::WinState::Draw);

		Ok(())
	}

	// capturing a planet and moving ships
	#[test]
	fn test_colliding_orders() -> anyhow::Result<()> {
		crate::ensure_tracing_subscriber();
		let params = universegen::UniverseParameters {
			star_systems_per_empire: 10,
			empires_n: 2,
			add_testables: true,
			generator: universegen::UniverseGenerator::Haphazard,
			fixed_population: Some(1e9 as u64),
			seed: Some(0),
			..Default::default()
		};
		let mut u = universegen::generate_universe(&params)?;
		let ship_cost = u.ship_cost;

		let empire_1_id = u.empires.iter().find(|e| e.1.name == "Empire1").unwrap().1.id;
		let e1 = action::Actor { empire_id: empire_1_id.cast() };

		let capital1 = u.star_systems.iter().find(|s| s.1.name == "StarSystem1").unwrap().1.id.cast();
		let capital2 = u.star_systems.iter().find(|s| s.1.name == "StarSystem2").unwrap().1.id.cast();
		u.getf_mut(empire_1_id)?.goods_storage.energy_credits = ship_cost * 8;

		let fleet1_action = action::Action::BuildFleets {
			star_system_id: capital1.cast(),
			fleets_number: 1,
		};

		assert!(u.set_actions(Some(vec![fleet1_action]), &e1).is_ok());

		u.make_turn()?;

		let ui = u.interface();

		let fleet1 = ui.empire(empire_1_id).fleets()[0].data.id;
		assert_eq!(ui.fleet(fleet1).star_system().data.id, capital1.cast());

		u.set_actions(
			Some(vec![action::Action::BuildShips {
				fleet_id: fleet1.cast(),
				ships_number: 3,
			}]),
			&e1,
		)?;

		u.make_turn()?;
		let actions = vec![action::Action::MoveFleet {
			fleet_id: fleet1.cast(),
			target_star_system_id: capital2,
		}];

		u.set_actions(Some(actions), &e1)?;

		u.make_turn()?;

		let mut actions = vec![];

		actions.extend(vec![action::Action::MoveFleet {
			fleet_id: fleet1.cast(),
			target_star_system_id: capital1,
		}]);
		actions.extend(vec![action::Action::MoveFleet {
			fleet_id: fleet1.cast(),
			target_star_system_id: capital2,
		}]);

		assert!(u.set_actions(Some(actions), &e1).is_err());

		Ok(())
	}
}
