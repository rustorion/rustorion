use crate::net::server;
use anyhow::{anyhow, bail, Context, Result};
use state_machine::AsyncEventMachine;
use state_machine::Handle;
use std::io;
use std::net::ToSocketAddrs;
use std::sync::Arc;
use tokio::net::TcpStream;
use tokio::sync::mpsc;
use tokio_rustls::{rustls::ClientConfig, TlsConnector};

#[derive(Debug, Clone)]
pub enum ClientInEvent {
	GetServerState,
	GetView,
	GetAffero,
	GetEmpireID,
	SetActions(Vec<crate::action::Action>),
	AssignPlayerToSlot(Option<server::PlayerID>, u64),
	AssignAiToSlot(u64),
	AddSlot,
	RemoveSlot(u64),
	StartGame,
}

#[derive(Debug, Clone)]
pub enum ClientEvent {
	GotAffero(String),
	GotServerState(crate::net::server::ServerStateView),
	StateChanged(u64),
	GotView(Box<crate::universeview::Universe>),
	GotEmpireID(crate::storage::ID<crate::universeview::Empire>),
	ActionsSet,
}

type ClientClient = cborpc::Client<tokio::io::ReadHalf<tokio_rustls::client::TlsStream<TcpStream>>, tokio::io::WriteHalf<tokio_rustls::client::TlsStream<TcpStream>>>;

struct ClientBackProtocol;

impl cborpc::Protocol for ClientBackProtocol {
	type State = mpsc::UnboundedSender<BackClientEvent>;
	fn name(&self) -> String {
		"rustorion-client-0".into()
	}
	fn method(&mut self, sender: &mut mpsc::UnboundedSender<BackClientEvent>, method_name: &str, message: &[u8]) -> Result<cborpc::CallResponse> {
		match method_name {
			"get_affero" => Ok(cborpc::CallResponse {
				success: true,
				message: ciborium::into_vec(crate::AFFERO)?,
			}),
			"state_changed" => {
				let state_n: u64 = ciborium::from_reader(message)?;
				sender.send(BackClientEvent::StateChanged(state_n))?;
				Ok(cborpc::CallResponse { success: true, message: vec![] })
			}
			_ => self.method_missing(method_name),
		}
	}
}

#[derive(Default)]
pub struct Client(pub state_machine::TokioDriver<Client>);

impl AsyncEventMachine for Client {
	type InEvent = ClientInEvent;
	type OutEvent = ClientEvent;
	type Driver = state_machine::TokioDriver<Self>;
}

impl Client {
	pub async fn connect(self, host: String, auth_data: crate::net::AuthData) -> Result<()> {
		let mut guts = self.0.guts;
		let handle = self.0.handle;
		let addr = host.to_socket_addrs()?.next().ok_or_else(|| io::Error::from(io::ErrorKind::NotFound))?;
		let mut back_addr = addr;
		back_addr.set_port(back_addr.port() + 1);

		let client_config = ClientConfig::builder_with_provider(Arc::new(rustls::crypto::ring::default_provider()))
			.with_safe_default_protocol_versions()
			.unwrap()
			.dangerous()
			.with_custom_certificate_verifier(Arc::new(NoVerifier {}))
			.with_client_auth_cert(vec![auth_data.cert.into()], auth_data.key.try_into().unwrap())?;
		// client_config
		let connector = TlsConnector::from(Arc::new(client_config.clone()));

		let tcp_stream = TcpStream::connect(&addr).await?;
		let domain = rustls::pki_types::ServerName::try_from("localhost").context("Failed DNS")?;
		let stream = connector.connect(domain, tcp_stream).await?;

		let (r, w) = tokio::io::split(stream);
		let mut cborpc_client = cborpc::Client::new(r, w, None);

		let (back_sender, mut back_receiver) = mpsc::unbounded_channel();
		let back_config = client_config.clone();
		let mut back_connection = tokio::task::spawn(Self::back_connect(back_config, back_addr, back_sender));

		loop {
			tokio::select! {
				in_event = guts.receiver.recv() => match in_event.unwrap() {
					// TODO: improve cborpc api and simplify this
					ClientInEvent::GetView => {
						let view = Self::get_view(&mut cborpc_client).await?;
						handle.broadcast_event(ClientEvent::GotView(Box::new(view)));
					},
					ClientInEvent::GetAffero => {
						let affero = Self::obtain_affero(&mut cborpc_client).await?;
						handle.broadcast_event(ClientEvent::GotAffero(affero));
					},
					ClientInEvent::GetServerState => {
						let state = Self::get_state(&mut cborpc_client).await?;
						handle.broadcast_event(ClientEvent::GotServerState(state));
					},
					ClientInEvent::GetEmpireID => {
						let id = Self::get_empire_id(&mut cborpc_client).await?;
						handle.broadcast_event(ClientEvent::GotEmpireID(id));
					},
					ClientInEvent::SetActions(actions) => {
						Self::set_actions(&mut cborpc_client, &actions).await?;
						handle.broadcast_event(ClientEvent::ActionsSet);
					}
					ClientInEvent::AddSlot => {
						Self::add_slot(&mut cborpc_client).await?;
					}
					ClientInEvent::RemoveSlot(slot_n) => {
						Self::remove_slot(&mut cborpc_client, slot_n).await?;
					}
					ClientInEvent::AssignPlayerToSlot(player_id_opt, slot_n) => {
						let cr = cborpc_client.call(&cborpc::MethodCall {
							protocol_name: "rustorion-server-0".to_string(),
							method_name: "assign_player_to_slot".to_string(),
							message: ciborium::into_vec(&(player_id_opt, slot_n))?,
						}).await?;
						if !cr.success {
							bail!("Call failed!");
						}

					}
					ClientInEvent::AssignAiToSlot(slot_n) => {
						let cr = cborpc_client.call(&cborpc::MethodCall {
							protocol_name: "rustorion-server-0".to_string(),
							method_name: "assign_ai_to_slot".to_string(),
							message: ciborium::into_vec(&slot_n)?,
						}).await?;
						if !cr.success {
							bail!("Call failed!");
						}
					}
					ClientInEvent::StartGame => {
						let cr = cborpc_client.call(&cborpc::MethodCall {
							protocol_name: "rustorion-server-0".to_string(),
							method_name: "start_game".to_string(),
							message: vec![],
						}).await?;
						if !cr.success {
							bail!("Call failed!");
						}
					}
				},
				back_event = back_receiver.recv() => {
					match back_event.unwrap() {
					BackClientEvent::StateChanged(n) => handle.broadcast_event(ClientEvent::StateChanged(n)),
					};
				},
				back_result = &mut back_connection => {
					bail!("Back-connection failed: {:?}", back_result);
				},
			};
		}
	}

	async fn add_slot(cborpc_client: &mut ClientClient) -> Result<()> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "add_slot".to_string(),
			message: vec![],
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		Ok(())
	}

	async fn remove_slot(cborpc_client: &mut ClientClient, slot_n: u64) -> Result<()> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "remove_slot".to_string(),
			message: ciborium::into_vec(&slot_n)?,
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}
		Ok(())
	}

	async fn get_view(cborpc_client: &mut ClientClient) -> Result<crate::universeview::Universe> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "get_view".to_string(),
			message: vec![],
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}

		let view = ciborium::from_reader(cr.message.as_slice())?;

		Ok(view)
	}

	async fn get_state(cborpc_client: &mut ClientClient) -> Result<server::ServerStateView> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "get_state".to_string(),
			message: vec![],
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}

		let state = ciborium::from_reader(cr.message.as_slice())?;

		Ok(state)
	}

	async fn get_empire_id(cborpc_client: &mut ClientClient) -> Result<crate::storage::ID<crate::universeview::Empire>> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "get_empire_id".to_string(),
			message: vec![],
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}

		let empire_id = ciborium::from_reader(cr.message.as_slice())?;

		Ok(empire_id)
	}

	async fn obtain_affero(cborpc_client: &mut ClientClient) -> Result<String> {
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "get_affero".to_string(),
			message: vec![],
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}

		Ok(ciborium::from_reader(cr.message.as_slice())?)
	}

	// TODO: set unready status
	async fn set_actions(cborpc_client: &mut ClientClient, actions: &[crate::action::Action]) -> Result<()> {
		let actions_vec = ciborium::into_vec(&actions)?;
		let mcall = cborpc::MethodCall {
			protocol_name: "rustorion-server-0".to_string(),
			method_name: "set_actions".to_string(),
			message: actions_vec,
		};
		let cr = cborpc_client.call(&mcall).await?;

		if !cr.success {
			return Err(anyhow!("Call failed!"));
		}

		Ok(())
	}

	/// Provide rustorion-client-0 interface to server
	/// TODO: track fails in this
	pub async fn back_connect(client_config: ClientConfig, back_addr: std::net::SocketAddr, event_channel: mpsc::UnboundedSender<BackClientEvent>) -> Result<()> {
		let connector = TlsConnector::from(Arc::new(client_config));

		let tcp_stream = TcpStream::connect(&back_addr).await?;
		let domain = rustls::pki_types::ServerName::try_from("localhost").context("Failed DNS")?;
		let stream = connector.connect(domain, tcp_stream).await?;
		let (recv, send) = tokio::io::split(stream);
		let mut responder = cborpc::Responder::new(event_channel, recv, send, None);
		responder.add_protocol(ClientBackProtocol);
		while let Some(response) = responder.answer_call().await? {
			tracing::debug!("Call answered success={}", response);
		}
		Ok(())
	}
}

#[derive(Debug, Clone)]
pub enum BackClientEvent {
	StateChanged(u64),
}

#[derive(Debug, Clone)]
struct NoVerifier;

impl rustls::client::danger::ServerCertVerifier for NoVerifier {
	fn verify_server_cert(
		&self,
		_end_entity: &rustls::pki_types::CertificateDer,
		_intermediates: &[rustls::pki_types::CertificateDer],
		_server_name: &rustls::pki_types::ServerName,
		_ocsp_response: &[u8],
		_now: rustls::pki_types::UnixTime,
	) -> std::result::Result<rustls::client::danger::ServerCertVerified, rustls::Error> {
		Ok(rustls::client::danger::ServerCertVerified::assertion())
	}

	fn verify_tls12_signature(&self, _: &[u8], _: &rustls::pki_types::CertificateDer<'_>, _: &rustls::DigitallySignedStruct) -> Result<rustls::client::danger::HandshakeSignatureValid, rustls::Error> {
		Ok(rustls::client::danger::HandshakeSignatureValid::assertion())
	}

	fn verify_tls13_signature(&self, _: &[u8], _: &rustls::pki_types::CertificateDer<'_>, _: &rustls::DigitallySignedStruct) -> Result<rustls::client::danger::HandshakeSignatureValid, rustls::Error> {
		Ok(rustls::client::danger::HandshakeSignatureValid::assertion())
	}

	fn supported_verify_schemes(&self) -> Vec<rustls::SignatureScheme> {
		vec![rustls::SignatureScheme::ED25519]
	}
}
