use crate::names;
use crate::storage::*;
use crate::units::*;
use serde::{Deserialize, Serialize};
use std::collections::hash_set::HashSet;
use thiserror::Error;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
pub enum Action {
	CaptureStarSystem {
		star_system_id: ID<crate::universeview::StarSystem>,
		empire_id: ID<crate::universeview::Empire>,
	},
	MoveFleet {
		fleet_id: ID<crate::universeview::Fleet>,
		target_star_system_id: ID<crate::universeview::StarSystem>,
	},
	BuildShips {
		fleet_id: ID<crate::universeview::Fleet>,
		ships_number: i64, // TODO: u64
	},
	BuildFleets {
		star_system_id: ID<crate::universeview::StarSystem>,
		fleets_number: u64,
	},
}

#[derive(Error, Debug)]
#[error("Following actions failed: {:?}", failures)]
pub struct ActionsFailed {
	pub failures: Vec<ActionFailure>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct ActionFailure {
	pub action: Action,
	pub error: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Actor {
	pub empire_id: ID<crate::universeview::Empire>,
}

/// Holds information that is required to detect collisions in user-submitted actions
#[derive(Default)]
pub struct ActionCache {
	pub star_systems_being_captured: HashSet<ID<crate::universeview::StarSystem>>,
	pub budget: Unit<EnergyCredit>,
	pub fleets_committed: HashSet<ID<crate::universeview::Fleet>>,
	actions: Vec<Action>,
}

impl ActionCache {
	pub fn new_batch(actions: Vec<Action>, universeview: &crate::universeview::Universe, actor: &Actor) -> Result<Self, ActionsFailed> {
		let mut failures = vec![];
		let mut action_cache = ActionCache::new(universeview);
		for action in actions {
			action_cache
				.add(action.clone(), universeview, actor)
				.map_err(|et| failures.push(ActionFailure { action, error: et.to_string() }))
				.unwrap_or(());
		}
		if failures.is_empty() {
			Ok(action_cache)
		} else {
			Err(ActionsFailed { failures })
		}
	}

	pub fn new(universeview: &crate::universeview::Universe) -> Self {
		let mut action_cache = ActionCache::default();
		action_cache.set_initial_values(universeview);
		action_cache
	}

	pub fn set_initial_values(&mut self, universeview: &crate::universeview::Universe) {
		self.budget = universeview.interface().controlled_empire().map(|e| e.data.goods_storage.energy_credits).unwrap_or_else(|| 0.into());
		self.actions = vec![];
		self.star_systems_being_captured = Default::default();
		self.fleets_committed = Default::default();
	}

	fn check_id<Entity: IDentified>(id: ID<Entity>, storage: &impl EntityStored<Entity>) -> Result<(), &'static str> {
		if storage.exists(id) {
			Ok(())
		} else {
			Err("ID not found in storage")
		}
	}

	pub fn actions(&self) -> &[Action] {
		&self.actions
	}

	pub fn actions_vec(self) -> Vec<Action> {
		self.actions
	}

	pub fn remove(&mut self, action: &Action, universeview: &crate::universeview::Universe, actor: &Actor) {
		let actions = self.actions.clone();
		self.set_initial_values(universeview);
		for action in actions.into_iter().filter(|other| other != action) {
			// TODO: report errors in result
			if let Err(err) = self.add(action, universeview, actor) {
				tracing::debug!("Action failed to readd after removal: {:?}", err);
			}
		}
	}

	// TODO: use interface here
	pub fn add(&mut self, action: Action, universeview: &crate::universeview::Universe, actor: &Actor) -> Result<(), &'static str> {
		match &action {
			Action::CaptureStarSystem { star_system_id, empire_id } => {
				Self::check_id(*star_system_id, universeview)?;
				Self::check_id(*empire_id, universeview)?;
				// Can only capture systems for yourself
				if *empire_id != actor.empire_id {
					return Err("Capturer is not the actor");
				}

				if universeview.star_systems_in_empires.parent(*star_system_id) == Some(actor.empire_id) {
					return Err("Can't capture own systems");
				}
				if self.star_systems_being_captured.contains(star_system_id) {
					return Err("Already being captured");
				}
				let fleets = universeview.fleets_in_star_systems.children(*star_system_id);
				let mut has_owned_fleets = 0;

				for fleet in fleets {
					let empire = universeview.fleets_in_empires.parent(fleet);
					if empire != Some(actor.empire_id) {
						return Err("Others' fleets are present, cannot capture the system");
					} else {
						has_owned_fleets += 1;
					}
				}
				if has_owned_fleets == 0 {
					return Err("Cannot capture a star system w/o any fleets");
				}

				let capture_price = universeview.interface().star_system(*star_system_id).capture_price();
				let new_budget = self.budget.sub(capture_price);

				if new_budget < 0.into() {
					return Err("Not enough money left");
				};

				self.budget = new_budget;
				self.star_systems_being_captured.insert(*star_system_id);
			}
			Action::MoveFleet { fleet_id, target_star_system_id } => {
				Self::check_id(*fleet_id, universeview)?;

				Self::check_id(*target_star_system_id, universeview)?;
				if universeview.fleets_in_empires.parent(*fleet_id) != Some(actor.empire_id) {
					return Err("Can't move others' fleets");
				}

				if self.fleets_committed.contains(fleet_id) {
					return Err("Fleet is already committed to an action");
				};

				// check if the target system is adjacent to the source system
				let current_ss = universeview.fleets_in_star_systems.parent(*fleet_id).unwrap();
				if !universeview.starlanes.linked(current_ss, *target_star_system_id) {
					return Err("Can't move fleets between non-linked star systems");
				}
				self.fleets_committed.insert(*fleet_id);
			}
			Action::BuildShips { fleet_id, ships_number } => {
				let total_cost = universeview.ship_cost.mul(ships_number);
				let new_budget = self.budget.sub(total_cost);

				if new_budget < 0.into() {
					return Err("Not enough money left");
				};

				let interface = universeview.interface();
				let fleet = interface.fleet(*fleet_id);
				if fleet.star_system().empire().map(|e| e.data.id) != Some(actor.empire_id) {
					return Err("Not owner of the system");
				}

				self.budget = new_budget;
			}
			Action::BuildFleets { fleets_number, .. } => {
				let new_budget = self.budget.sub(universeview.interface().fleet_cost().mul(i64::try_from(*fleets_number).unwrap()));

				if new_budget < 0.into() {
					return Err("Not enough money left");
				};
			}
		}
		self.actions.push(action);
		Ok(())
	}
}

/// Represents the action player decides to take, like capturing a planet.
/// Actions should be able to be verified to be sane by client, if server receives them and they fail to validate, it is a general error.
/// When all players have submitted their actions, they are applied (this cannot fail, just can produce different results) and turn into various pending situations which are handled by universe updating routine.
impl Action {
	pub fn apply(&self, universe: &mut crate::universe::Universe, actor: &Actor) {
		match self {
			Action::CaptureStarSystem { star_system_id, empire_id } => {
				let empire_id: ID<crate::universe::Empire> = empire_id.cast();
				let star_system_id = star_system_id.cast();
				let capture_price = universe.interface().star_system(star_system_id).capture_price();

				let empire = universe.getf_mut(empire_id).unwrap();
				let next_credits = empire.goods_storage.energy_credits.sub(capture_price);
				empire.goods_storage.energy_credits = if next_credits >= 0.into() {
					next_credits
				} else {
					tracing::warn!("Not enough credits for capturing, failing");
					return;
				};

				universe.set_star_system_owner(star_system_id, empire_id);
			}
			Action::MoveFleet { fleet_id, target_star_system_id } => {
				let fleet_id = fleet_id.cast();
				let target_star_system_id = target_star_system_id.cast();
				if let Some(current_ss) = universe.fleets_in_star_systems.parent(fleet_id) {
					universe.fleets_in_star_systems.del_link(current_ss, fleet_id);
					universe.fleets_in_star_systems.add_link(target_star_system_id, fleet_id);
				}
			}
			Action::BuildShips { fleet_id, ships_number } => {
				// TODO: check money
				let fleet_id = fleet_id.cast();
				let total_cost = universe.ship_cost.mul(ships_number);
				for _ in 0..*ships_number {
					let name = names::generate_name(&mut rand::thread_rng(), 5, "en", names::NameGenerator::Autistic);
					universe.add_ship(&name, fleet_id).unwrap();
				}
				let empire: &mut crate::universe::Empire = universe.getf_mut(actor.empire_id.cast()).unwrap();

				empire.goods_storage.energy_credits.sub_assign(total_cost);
			}
			Action::BuildFleets { star_system_id, fleets_number } => {
				// TODO: check money
				let star_system_id = star_system_id.cast();

				let total_cost = universe.interface().fleet_cost().mul(i64::try_from(*fleets_number).unwrap());

				let empire: &mut crate::universe::Empire = universe.getf_mut(actor.empire_id.cast()).unwrap();

				empire.goods_storage.energy_credits.sub_assign(total_cost);
				for _ in 0..*fleets_number {
					let name = names::generate_name(&mut rand::thread_rng(), 5, "or", names::NameGenerator::Autistic);
					let fleet_id = universe.add_fleet(&name, star_system_id, actor.empire_id.cast()).unwrap();
					universe.add_ship(&format!("{name} flagship"), fleet_id).unwrap();
				}
			}
		}
	}

	pub fn describe(&self, universe_view: &crate::universeview::Universe) -> String {
		match self {
			Action::CaptureStarSystem { star_system_id, .. } => {
				format!("Capture star system {}", universe_view.getf(*star_system_id).unwrap().name)
			}
			Action::MoveFleet { fleet_id, target_star_system_id, .. } => {
				let fleet_name = &universe_view.getf(*fleet_id).unwrap().name;
				format!("Move fleet {fleet_name} to star system {}", universe_view.getf(*target_star_system_id).unwrap().name)
			}
			Action::BuildShips { fleet_id, ships_number } => {
				let fleet_name = &universe_view.getf(*fleet_id).unwrap().name;
				format!("Build {ships_number} ships for fleet {fleet_name}")
			}
			Action::BuildFleets { star_system_id, fleets_number } => {
				format!("Build {} fleets at {}", fleets_number, universe_view.getf(*star_system_id).unwrap().name)
			}
		}
	}
}
