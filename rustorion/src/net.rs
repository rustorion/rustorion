pub mod client;
pub mod server;

use anyhow::{Context, Result};

pub struct AuthData {
	pub cert: Vec<u8>,
	pub key: Vec<u8>,
}

impl AuthData {
	pub fn obtain_by_name(name: &str) -> Result<AuthData> {
		crate::names::sanity_check(name)?;
		let auth_data_dir = crate::project_dir()?.data_dir().join("auth_data");
		let cert_path = auth_data_dir.join(format!("{}.cert", name));
		let key_path = auth_data_dir.join(format!("{}.key", name));
		match std::fs::read(&cert_path).and_then(|x| Ok((x, std::fs::read(&key_path)?))) {
			Ok((cert, key)) => Ok(AuthData { cert, key }),
			Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {
				std::fs::create_dir_all(&auth_data_dir)?;
				let auth_data = AuthData::generate();
				std::fs::write(&cert_path, &auth_data.cert).context("failed to write certificate")?;
				std::fs::write(&key_path, &auth_data.key).context("failed to write private key")?;
				Ok(auth_data)
			}
			Err(e) => Err(e).context("error reading auth data"),
		}
	}

	pub fn generate() -> AuthData {
		let key_pair = rcgen::KeyPair::generate_for(&rcgen::PKCS_ED25519).unwrap();
		let params = rcgen::CertificateParams::new([String::from("localhost")]).unwrap();
		let cert = params.self_signed(&key_pair).unwrap();
		let key = key_pair.serialize_der();
		let cert = cert.der().to_vec();
		AuthData { cert, key }
	}

	pub fn player_id(&self) -> server::PlayerID {
		blake3::hash(&self.cert).into()
	}
}
